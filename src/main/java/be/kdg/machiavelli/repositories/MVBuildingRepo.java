package be.kdg.machiavelli.repositories;

import be.kdg.machiavelli.model.game.artifacts.MVBuilding;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MVBuildingRepo extends JpaRepository<MVBuilding,Integer> {
    @Override
    List<MVBuilding> findAll();

    @Override
    <S extends MVBuilding> List<S> saveAll(Iterable<S> entities);

    @Override
    MVBuilding getOne(Integer integer);
}
