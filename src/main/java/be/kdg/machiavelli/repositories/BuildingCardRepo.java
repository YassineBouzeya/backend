package be.kdg.machiavelli.repositories;

import be.kdg.machiavelli.model.game.artifacts.BuildingCard;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BuildingCardRepo extends JpaRepository<BuildingCard, Integer> {
    @Override
    List<BuildingCard> findAll();

    @Override
    <S extends BuildingCard> List<S> saveAll(Iterable<S> entities);

    @Override
    BuildingCard getOne(Integer integer);


}
