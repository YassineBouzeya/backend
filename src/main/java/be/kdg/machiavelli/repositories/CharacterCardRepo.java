package be.kdg.machiavelli.repositories;

import be.kdg.machiavelli.model.game.artifacts.CharacterCard;
import be.kdg.machiavelli.model.game.artifacts.MVCharacter;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CharacterCardRepo extends JpaRepository<CharacterCard, Integer> {

    @Override
    List<CharacterCard> findAll();

    @Override
    <S extends CharacterCard> List<S> saveAll(Iterable<S> entities);

    @Override
    CharacterCard getOne(Integer integer);

   // List<CharacterCard> findAllByMvGameId(int id);
}
