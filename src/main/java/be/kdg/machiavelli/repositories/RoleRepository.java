package be.kdg.machiavelli.repositories;

import be.kdg.machiavelli.model.security.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role,Long> {
}
