package be.kdg.machiavelli.repositories;

import be.kdg.machiavelli.model.user.Friend;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FriendRepo extends JpaRepository<Friend,Integer> {
    Friend findByUserName(String username);
}
