package be.kdg.machiavelli.repositories;

import be.kdg.machiavelli.model.security.User;
import be.kdg.machiavelli.model.user.FriendRequest;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FriendRequestRepo extends JpaRepository<FriendRequest,Integer> {
    List<FriendRequest> findAllByReceiver(User receiver);
    List<FriendRequest> findAllBySender(User receiver);
    FriendRequest findBySenderAndReceiver(User sender, User receiver);
}
