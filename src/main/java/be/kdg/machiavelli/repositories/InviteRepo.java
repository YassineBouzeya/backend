package be.kdg.machiavelli.repositories;

import be.kdg.machiavelli.model.security.User;
import be.kdg.machiavelli.model.user.Invite;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface InviteRepo extends JpaRepository<Invite, Integer> {
    List<Invite> findAllByInvitedUser(User user);
}
