package be.kdg.machiavelli.repositories;

import be.kdg.machiavelli.model.user.Replay;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReplayRepo extends JpaRepository<Replay, Integer> {
}
