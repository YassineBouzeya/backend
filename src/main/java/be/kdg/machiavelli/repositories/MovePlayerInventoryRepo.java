package be.kdg.machiavelli.repositories;

import be.kdg.machiavelli.model.game.logic.MovePlayerInventory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MovePlayerInventoryRepo extends JpaRepository<MovePlayerInventory,Integer> {
}
