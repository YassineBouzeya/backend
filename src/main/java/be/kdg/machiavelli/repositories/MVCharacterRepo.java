package be.kdg.machiavelli.repositories;

import be.kdg.machiavelli.model.game.artifacts.MVCharacter;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MVCharacterRepo extends JpaRepository<MVCharacter, Integer> {
}
