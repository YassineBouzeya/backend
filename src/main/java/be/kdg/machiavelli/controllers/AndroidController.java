package be.kdg.machiavelli.controllers;

import be.kdg.machiavelli.model.dto.AndroidDataDto;
import be.kdg.machiavelli.model.dto.BuildingCardDto;
import be.kdg.machiavelli.model.dto.CharacterCardDto;
import be.kdg.machiavelli.services.CharacterCardService;
import be.kdg.machiavelli.services.GameService;
import be.kdg.machiavelli.services.LobbyService;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
public class AndroidController {
    private final LobbyService lobbyService;
    private final SimpMessagingTemplate template;
    private GameService gameService;
    private CharacterCardService characterCardService;

    public AndroidController(LobbyService lobbyService, GameService gameService, SimpMessagingTemplate template, CharacterCardService characterCardService) {
        this.lobbyService = lobbyService;
        this.gameService = gameService;
        this.template = template;
        this.characterCardService = characterCardService;
    }

    @MessageMapping("/app/android/connect/{gameId}")
    public void androidConnect(Principal principal, @DestinationVariable int gameId) {
        boolean canConnect = this.lobbyService.connectAndroid(principal.getName(), gameId);
        this.template.convertAndSend("/r/android/connect/" + principal.getName() + "/" + gameId, canConnect);
    }

    @MessageMapping("/app/android/myTurn/{gameId}")
    public void androidClientTurn(Principal principal, @DestinationVariable int gameId, AndroidDataDto androidDataDto) {
        this.template.convertAndSend("/r/android/myTurn/" + principal.getName() + "/" + gameId, androidDataDto);
    }


    @MessageMapping("/app/android/characterSelected/{gameId}")
    public void androidCharacterSelected(Principal principal, CharacterCardDto chosenCharacter, @DestinationVariable int gameId) {
        CharacterCardDto characterCardDto = this.characterCardService.getOneCharacterCardDto(chosenCharacter.getId());
        this.template.convertAndSend("/r/android/characterSelected/" + principal.getName() + "/" + gameId, characterCardDto);
    }

    @MessageMapping("/app/android/coinsClick/{gameId}")
    public void androidCoinsClick(Principal principal, @DestinationVariable int gameId) {
        this.template.convertAndSend("/r/android/coinsClick/" + principal.getName() + "/" + gameId, "");
    }

    @MessageMapping("/app/android/cardsClick/{gameId}")
    public void androidCardsClick(Principal principal, @DestinationVariable int gameId) {
        this.template.convertAndSend("/r/android/cardsClick/" + principal.getName() + "/" + gameId, "");
    }

    @MessageMapping("/app/android/buildingCards/{gameId}")
    public void androidBuildingCards(Principal principal, @DestinationVariable int gameId, AndroidDataDto androidDataDto) {
        this.template.convertAndSend("/r/android/buildingCards/" + principal.getName() + "/" + gameId, androidDataDto);
    }
    @MessageMapping("/app/android/buildingSelected/{gameId}")
    public void androidbuildingSelected(Principal principal, @DestinationVariable int gameId, BuildingCardDto buildingCardDto) {
        this.template.convertAndSend("/r/android/buildingSelected/" + principal.getName() + "/" + gameId, buildingCardDto);
    }
}
