package be.kdg.machiavelli.controllers;

import be.kdg.machiavelli.model.dto.GameDto;
import be.kdg.machiavelli.model.game.artifacts.CharacterCard;
import be.kdg.machiavelli.services.BuildingService;
import be.kdg.machiavelli.services.CharacterService;
import be.kdg.machiavelli.services.GameService;
import be.kdg.machiavelli.services.LobbyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.List;

/**
 * HttpStatus.I_AM_A_TEAPOT indicates an Exception happened in the chain of controller - service - repo
 * The Exception gets logged in this class (the Controller)
 */

@RestController
public class CardsController {
    private static final Logger LOGGER = LoggerFactory.getLogger(CardsController.class);

    private final CharacterService characterService;
    private GameService gameService;
    private LobbyService lobbyService;
    private BuildingService buildingService;

    @Autowired
    public CardsController(CharacterService characterService,
                           GameService gameService, LobbyService lobbyService,
                           BuildingService buildingService) {
        this.characterService = characterService;
        this.gameService = gameService;
        this.lobbyService=lobbyService;
        this.buildingService=buildingService;
    }

    //@CrossOrigin("http://localhost:4200")
    @GetMapping("characters/{id}")
    public ResponseEntity getCharactersOfGame(@PathVariable int id) {
        try {
            return new ResponseEntity(gameService.getCharacterCardDtosOfGame(id), HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("Exception in getCharactersOfGame: " + e.getMessage());
            return new ResponseEntity(HttpStatus.I_AM_A_TEAPOT);
        }
    }


    @GetMapping("buildings/{id}")
    public ResponseEntity getBuildingsOfGame(@PathVariable int id) {
        try {
            return new ResponseEntity(buildingService.getBuildingsCardDtosOfGame(id), HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("Exception in getBuildingsOfGame: " + e.getMessage());
            return new ResponseEntity(HttpStatus.I_AM_A_TEAPOT);
        }
    }

  @MessageMapping("/app/charactercards")
  @SendToUser("/r/charactercards")
  public List<CharacterCard> getCharacterCards(Principal principal, GameDto gameDto) throws Exception {
      //MVGame mvGame= this.lobbyService.makeLobby(principal.getName(),gameDto);
      return null;
  }
}
