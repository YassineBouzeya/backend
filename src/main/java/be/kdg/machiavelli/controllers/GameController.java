package be.kdg.machiavelli.controllers;

import be.kdg.machiavelli.model.dto.*;
import be.kdg.machiavelli.services.CharacterCardService;
import be.kdg.machiavelli.services.GameService;
import be.kdg.machiavelli.services.LobbyService;
import be.kdg.machiavelli.services.PlayerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * HttpStatus.I_AM_A_TEAPOT indicates an Exception happened in the chain of controller - service - repo
 * The Exception gets logged in this class (the Controller)
 */

@RestController
public class GameController {
    private static final Logger LOGGER = LoggerFactory.getLogger(GameController.class);

    private final GameService gameService;
    private final PlayerService playerService;
    private final CharacterCardService characterCardService;
    private final SimpMessagingTemplate template;
    private LobbyService lobbyService;

    public GameController(GameService gameService, CharacterCardService characterCardService, SimpMessagingTemplate template, LobbyService lobbyService, PlayerService playerService) {
        this.gameService = gameService;
        this.characterCardService = characterCardService;
        this.template = template;
        this.lobbyService = lobbyService;
        this.playerService = playerService;
    }

    //returns all in lobby games with JoinType OPEN
    @GetMapping("searchGame")
    public ResponseEntity searchGames() {
        try {
            return new ResponseEntity(this.gameService.searchGameDtos().parallelStream()
                    .filter((mvGame) -> mvGame.getMaxPlayers() > mvGame.getNrOfPlayers()).collect(Collectors.toList()),
                    HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("Exception in searchGames: " + e.getMessage());
            return new ResponseEntity(HttpStatus.I_AM_A_TEAPOT);
        }
    }

    //returns all games where a user plays/has played in
    @GetMapping("gameOverview")
    public ResponseEntity getGameOverview(Principal principal) {
        try {
            return new ResponseEntity(this.gameService.getGameOverview(principal.getName()), HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("Exception in getGameOverview: " + e.getMessage());
            return new ResponseEntity(HttpStatus.I_AM_A_TEAPOT);
        }
    }

    @GetMapping("getGame/{gameId}")
    public ResponseEntity getGame(@PathVariable int gameId) {
        try {
            return new ResponseEntity(this.gameService.getGameDto(gameId), HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("Exception in getGame: " + e.getMessage());
            return new ResponseEntity(HttpStatus.I_AM_A_TEAPOT);
        }
    }


    @GetMapping("getLiteGame/{gameId}")
    public ResponseEntity getLiteGame(@PathVariable int gameId) {
        try {
            return new ResponseEntity(this.gameService.getLiteGameDto(gameId), HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("Exception in getLiteGame: " + e.getMessage());
            return new ResponseEntity(HttpStatus.OK);
        }
    }

    @MessageMapping("/app/assignCharacter")
    public GameDto assignCharacterToPlayer(@RequestBody ChangeCaracterDto changeCaracterDto) {
/*
        if (gameService.asignCharachterToPlayer(changeCaracterDto.getGameId(), changeCaracterDto.getPlayerId(), changeCaracterDto.getCharacterCardId())) {
            return gameService.getGameDto(changeCaracterDto.getGameId());
            //this.template.convertAndSend("/r/startGame/" + gameDto.getId(), "");
        }else {
            return null;
        }
  */
        return null;
    }

    @MessageMapping("/app/simpleRefresh")
    public void refreshGame(ChangeCaracterDto change) {
        change.getGameDto().setPlayerTurn(gameService.nextPlayer(change.getGameDto(), change.getPlayerId()));
        this.template.convertAndSend("/r/refreshGame/" + change.getGameDto().getId(), change.getGameDto());
        this.myTurnNotification(change.getGameDto());
        this.otherPlayedNotification(change.getGameDto());
    }


    @MessageMapping("/app/addCoins")
    public void addCoins(ChangeCaracterDto change) {
        this.template.convertAndSend("/r/refreshGame/" + change.getGameDto().getId(), change.getGameDto());
    }

    @MessageMapping("/app/fastRefresh")
    public void fastRefresh(ChangeCaracterDto change) {
        this.template.convertAndSend("/r/fastRefresh/" + change.getGameDto().getId(), change.getGameDto());
    }

    @MessageMapping("/app/characterSelected")
    public void characterSelected(ChangeCaracterDto change) {
        GameDto gameDto = this.gameService.assignCharacterToPlayer(change);
        this.template.convertAndSend("/r/receiveChange/" + change.getGameId(), gameDto);
        this.gameService.saveGame(change.getGameDto());
        this.myTurnNotification(gameDto);
        this.otherPlayedNotification(gameDto);
    }


    //Notification for the player whose turn it is
    private void myTurnNotification(GameDto gameDto) {
        Arrays.stream(gameDto.getPlayers())
                .filter(p -> p.getOrderNr() == gameDto.getPlayerTurn())
                .findAny()
                .ifPresent(
                        turnPlayer -> this.template.convertAndSendToUser(turnPlayer.getUserName(), "/r/myTurn", gameService.getLiteGameDto(gameDto.getId())));
    }

    //Notification that someone has played
    private void otherPlayedNotification(GameDto gameDto) {
        List<PlayerDto> otherPlayers = Arrays.stream(gameDto.getPlayers())
                .filter(p -> p.getOrderNr() != gameDto.getPlayerTurn() && p.getOrderNr() != gameDto.getPlayerTurn() - 1).collect(Collectors.toList());
        for (PlayerDto otherPlayer : otherPlayers) {
            this.template.convertAndSendToUser(otherPlayer.getUserName(), "/r/otherPlayed", gameDto);
        }
    }

    @MessageMapping("/app/nextCharacter")
    public void nextCharacter(ChangeCaracterDto change) {
        GameDto gameDto = this.gameService.nextCharacter(change.getGameDto());
        if (gameDto.getCharacterTurn() == -1) {
            this.template.convertAndSend("/r/resetRound/" + change.getGameId(), gameDto);
        } else {
            this.template.convertAndSend("/r/refreshGame/" + change.getGameId(), gameDto);
        }
        this.gameService.saveGame(change.getGameDto());
    }

    @GetMapping("getPlayersFromGame/{gameId}")
    public ResponseEntity getGamePlayers(@PathVariable int gameId) {
        try {
            return new ResponseEntity(this.playerService.getPlayersFromGame(gameId), HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("Exception in getGame: " + e.getMessage());
            return new ResponseEntity(HttpStatus.I_AM_A_TEAPOT);
        }
    }
}