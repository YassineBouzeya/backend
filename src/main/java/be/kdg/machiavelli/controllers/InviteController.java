package be.kdg.machiavelli.controllers;

import be.kdg.machiavelli.model.dto.InviteDto;
import be.kdg.machiavelli.services.InviteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import java.security.Principal;

/**
 * HttpStatus.I_AM_A_TEAPOT indicates an Exception happened in the chain of controller - service - repo
 * The Exception gets logged in this class (the Controller)
 */

@RestController
public class InviteController {
    private static final Logger LOGGER = LoggerFactory.getLogger(InviteController.class);

    private final SimpMessagingTemplate template;
    private InviteService inviteService;

    public InviteController(SimpMessagingTemplate template, InviteService inviteService) {
        this.template = template;
        this.inviteService = inviteService;
    }

    @MessageMapping("/app/invite")
    public void invite(Principal principal, InviteDto invite) {
        int gameId = invite.getGameDto().getId();
        String friendName = invite.getInvitedUserName();
        InviteDto inviteDto = new InviteDto();
        try {
            inviteDto = this.inviteService.invite(principal.getName(), friendName ,gameId);
        }
        catch (UsernameNotFoundException e){
            LOGGER.error(e.toString());
        }

        this.template.convertAndSendToUser(friendName,"/r/invite", inviteDto);
    }

    @GetMapping("getInvites")
    public ResponseEntity getInvites(Principal principal) {
        try {
            return new ResponseEntity(this.inviteService.getInvites(principal.getName()), HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("Exception in getInvites: " + e.getMessage());
            return new ResponseEntity(HttpStatus.I_AM_A_TEAPOT);
        }
    }

    @PostMapping("removeInvite")
    public ResponseEntity removeInvite(@RequestBody InviteDto inviteDto) {
        try {
            this.inviteService.removeInvite(inviteDto.getId());
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.I_AM_A_TEAPOT);
        }
    }

}
