package be.kdg.machiavelli.controllers;

import be.kdg.machiavelli.model.dto.GameDto;
import be.kdg.machiavelli.model.dto.PlayerDto;
import be.kdg.machiavelli.services.GameService;
import be.kdg.machiavelli.services.LobbyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;

/**
 * HttpStatus.I_AM_A_TEAPOT indicates an Exception happened in the chain of controller - service - repo
 * The Exception gets logged in this class (the Controller)
 */

@RestController
public class LobbyController {
    private static final Logger LOGGER = LoggerFactory.getLogger(LobbyController.class);

    private final LobbyService lobbyService;
    private GameService gameService;
    private final SimpMessagingTemplate template;

    @Autowired
    public LobbyController(LobbyService lobbyService, SimpMessagingTemplate template, GameService gameService) {
        this.lobbyService = lobbyService;
        this.template = template;
        this.gameService = gameService;
    }

    @PostMapping("makeLobby")
    public ResponseEntity makeLobby(@RequestBody GameDto gameDto) throws Exception {
        try {
            gameDto = this.lobbyService.makeLobby(gameDto);
            return new ResponseEntity(gameDto, HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("Exception in makeLobby: " + e.getMessage());
            return new ResponseEntity(HttpStatus.I_AM_A_TEAPOT);
        }
    }

    //add joining user's player to game and return the game player's userNames
    @MessageMapping("/app/joinLobby")
    public void joinLobby(Principal principal, GameDto gameDto) {
        ArrayList<PlayerDto> playerDtos = this.lobbyService.joinLobby(principal.getName(), gameDto.getId());
        this.template.convertAndSend("/r/lobby/" + gameDto.getId(), playerDtos);
    }

    @MessageMapping("/app/startGame")
    public void startGame(GameDto gameDto) {
        this.lobbyService.startGame(gameDto.getId());
        this.template.convertAndSend("/r/startGame/" + gameDto.getId(), gameService.getGameDto(gameDto.getId()));

    }

    @GetMapping("getLobbyPlayers/{gameId}")
    public ResponseEntity getLobbyPlayers(@PathVariable int gameId){
        try {
            return new ResponseEntity(this.lobbyService.getLobbyPlayers(gameId), HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("Exception in getLobbyPlayers: " + e.getMessage());
            return new ResponseEntity(HttpStatus.I_AM_A_TEAPOT);
        }
    }

    // To permanently leave a game/lobby (ongoing game should end when player leaves and show feedback)
    @MessageMapping("/app/leaveGame")
    public void leaveGame(Principal principal, GameDto gameDto) {
        PlayerDto leavingPlayer = this.lobbyService.leaveGame(principal.getName(), gameDto.getId());
        this.template.convertAndSend("/r/leaveLobby/" + gameDto.getId(), leavingPlayer);
        this.template.convertAndSend("/r/leaveGame/" + gameDto.getId(), leavingPlayer);
    }

    @GetMapping("getPreviousPlayers/{username}")
    public ResponseEntity getPreviousPlayers(@PathVariable String username) {
        try {
            return new ResponseEntity(this.lobbyService.getPreviousPlayers(username), HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("Exception in getPreviousPlayers: " + e.getMessage());
            return new ResponseEntity(HttpStatus.I_AM_A_TEAPOT);
        }
    }


}