package be.kdg.machiavelli.controllers;

import be.kdg.machiavelli.model.dto.AndroidNotificationDto;
import be.kdg.machiavelli.model.dto.DeviceDto;
import be.kdg.machiavelli.model.dto.SettingsDto;
import be.kdg.machiavelli.model.dto.UserDto;
import be.kdg.machiavelli.model.security.User;
import be.kdg.machiavelli.services.AndroidNotificationService;
import be.kdg.machiavelli.services.UserService;
import net.minidev.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * HttpStatus.I_AM_A_TEAPOT indicates an Exception happened in the chain of controller - service - repo
 * The Exception gets logged in this class (the Controller)
 */

@RestController
public class UserController {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;
    @Autowired
    private AndroidNotificationService androidNotificationService;

    private final SimpMessagingTemplate template;

    public UserController(SimpMessagingTemplate template) {
        this.template = template;
    }

    @PostMapping("/register")
    public ResponseEntity registerUser(@RequestBody UserDto newUser) {
        try {
            LOGGER.info("User registered");
            this.userService.createNewUser(newUser);
            return new ResponseEntity("User registered", HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("Exception in registerUser: " + e.getMessage());
            return new ResponseEntity(HttpStatus.I_AM_A_TEAPOT);
        }

    }

    @GetMapping("/checkusername/{username}")
    public ResponseEntity checkU(@PathVariable String username) {
        try {
            return new ResponseEntity(this.userService.userNameBusy(username), HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("Exception in checkU: " + e.getMessage());
            return new ResponseEntity(HttpStatus.I_AM_A_TEAPOT);
        }
    }

    @GetMapping("/checkemail/{email}")
    public ResponseEntity checkE(@PathVariable String email) {
        try {
            return new ResponseEntity(this.userService.emailBusy(email), HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("Exception in checkE: " + e.getMessage());
            return new ResponseEntity(HttpStatus.I_AM_A_TEAPOT);
        }
    }

    @GetMapping("/getStatistics/{username}")
    public ResponseEntity getStatistics(@PathVariable String username) {
        try {
            return new ResponseEntity(userService.getStatisticsDto(username), HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("Exception in getStatistics: " + e.getMessage());
            return new ResponseEntity(HttpStatus.I_AM_A_TEAPOT);
        }
    }

    @GetMapping("/getUser")
    public ResponseEntity getUser(Principal principal) {
        try {
            return new ResponseEntity(this.userService.getUser(principal.getName()), HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("Exception in getUser: " + e.getMessage());
            return new ResponseEntity(HttpStatus.I_AM_A_TEAPOT);
        }
    }

    @GetMapping("/getUser/{username}")
    public ResponseEntity getUser(@PathVariable String username) {
        try {
            return new ResponseEntity(this.userService.getUser(username), HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("Exception in getUserWithUsername: " + e.getMessage());
            return new ResponseEntity(HttpStatus.I_AM_A_TEAPOT);
        }
    }

    @GetMapping("/getUsersContainingUsername/{username}")
    public ResponseEntity getUsersContainingUsername(@PathVariable String username) {
        try {
            return new ResponseEntity(this.userService.getUsersContainingUsername(username), HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("Exception in getUsersContainingUsername: " + e.getMessage());
            return new ResponseEntity(HttpStatus.I_AM_A_TEAPOT);
        }
    }

    @PostMapping("/saveUser")
    public ResponseEntity saveUser(@RequestBody UserDto userDto) {
        try {
            return new ResponseEntity(this.userService.saveUserWithDto(userDto), HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("Exception in saveUser: " + e.getMessage());
            return new ResponseEntity(HttpStatus.I_AM_A_TEAPOT);
        }
    }

    @PostMapping("/sendDeviceId")
    public ResponseEntity sendDeviceId(@RequestBody DeviceDto deviceDto) {
        try {
            return new ResponseEntity(this.userService.saveDeviceId(deviceDto), HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("Exception in sendDeviceId: " + e.getMessage());
            return new ResponseEntity(HttpStatus.I_AM_A_TEAPOT);
        }
    }

    @PostMapping("/sendAndroidNotification")
    public ResponseEntity sendAndroidNotification(@RequestBody AndroidNotificationDto androidNotificationDto) {
        try {
            User user = this.userService.findByUsername(androidNotificationDto.getUsername());
            if (user.getDeviceId() == null) {
                LOGGER.warn("No deviceId found for username: " + user.getUsername());
                return new ResponseEntity(HttpStatus.OK);
            } else {
                LOGGER.info("Sending message to deviceId " + user.getDeviceId());
                ResponseEntity response = send(androidNotificationDto, user.getDeviceId());
                if (response.getStatusCode().is2xxSuccessful()) {
                    LOGGER.info("Message sent");
                    return new ResponseEntity(HttpStatus.OK);
                } else {
                    return new ResponseEntity("Push notification failed", HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }
        } catch (Exception e) {
            LOGGER.error("Exception in getUser: " + e.getMessage());
            return new ResponseEntity(HttpStatus.I_AM_A_TEAPOT);
        }
    }

    @RequestMapping(value = "/send", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<String> send(AndroidNotificationDto androidNotificationDto, String deviceId) {

        JSONObject body = new JSONObject();
        body.put("to", deviceId);
        body.put("priority", "high");

        JSONObject data = new JSONObject();
        data.put("id", androidNotificationDto.getGameId());
        data.put("title", androidNotificationDto.getGameTitle());
        data.put("body", androidNotificationDto.getContent());

        body.put("data", data);

        HttpEntity<String> request = new HttpEntity<>(body.toString());

        CompletableFuture<String> pushNotification = androidNotificationService.send(request);
        CompletableFuture.allOf(pushNotification).join();

        try {
            String firebaseResponse = pushNotification.get();

            return new ResponseEntity<>(firebaseResponse, HttpStatus.OK);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return new ResponseEntity<>("Push Notification ERROR!", HttpStatus.BAD_REQUEST);
    }

    @PostMapping("/saveSettings")
    public ResponseEntity saveSettings(@RequestBody SettingsDto settingsDto) {
        try {
            return new ResponseEntity(this.userService.saveSettings(settingsDto), HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("Exception in saveSettings: " + e.getMessage());
            return new ResponseEntity(HttpStatus.I_AM_A_TEAPOT);
        }
    }

    @GetMapping("/getSettings")
    public ResponseEntity getSettings(Principal principal) {
        try {
            return new ResponseEntity(this.userService.getSettings(principal.getName()), HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("Exception in getSettings: " + e.getMessage());
            return new ResponseEntity(HttpStatus.I_AM_A_TEAPOT);
        }
    }

    @GetMapping("/getPrivacySettings/{username}")
    public ResponseEntity getPrivacySettings(@PathVariable String username) {
        try {
            return new ResponseEntity(this.userService.getPrivacySettings(username), HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("Exception in getPrivacySettings: " + e.getMessage());
            return new ResponseEntity(HttpStatus.I_AM_A_TEAPOT);
        }
    }
}
