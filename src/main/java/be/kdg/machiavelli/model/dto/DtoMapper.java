package be.kdg.machiavelli.model.dto;

import be.kdg.machiavelli.model.game.artifacts.BuildingCard;
import be.kdg.machiavelli.model.game.artifacts.CharacterCard;
import be.kdg.machiavelli.model.game.artifacts.MVBuilding;
import be.kdg.machiavelli.model.game.artifacts.MVCharacter;
import be.kdg.machiavelli.model.game.logic.MVGame;
import be.kdg.machiavelli.model.game.logic.MVPlayer;
import be.kdg.machiavelli.model.security.User;
import be.kdg.machiavelli.model.user.ChatMessage;
import be.kdg.machiavelli.model.user.Friend;
import be.kdg.machiavelli.model.user.FriendRequest;

import be.kdg.machiavelli.model.user.Invite;
import org.springframework.stereotype.Component;

@Component
public class DtoMapper {
    public DtoMapper() {
    }

    public GameDto toDto(MVGame mvGame) {
        GameDto result = new GameDto();
        result.setId(mvGame.getId());
        BuildingCardDto[] buildingCardDtos = new BuildingCardDto[mvGame.getBuildings().size()];
        for (int i = 0; i < mvGame.getBuildings().size(); i++) {
            buildingCardDtos[i] = toDto(mvGame.getBuildings().get(i));
        }
        result.setBuildingCards(buildingCardDtos);
        CharacterCardDto[] characterCardDtos = new CharacterCardDto[mvGame.getCharacters().size()];
        for (int i = 0; i < mvGame.getCharacters().size(); i++) {
            characterCardDtos[i] = toDto(mvGame.getCharacters().get(i));
        }
        PlayerDto[] playerDtos = new PlayerDto[mvGame.getPlayers().size()];
        for (int i = 0; i < mvGame.getPlayers().size(); i++) {
            playerDtos[i] = toDto(mvGame.getPlayers().get(i));
        }
        result.setPlayers(playerDtos);
        result.setCharacterCards(characterCardDtos);
        result.setGameName(mvGame.getName());
        result.setJoinType(mvGame.getJoinType().getI());
        result.setStatus(mvGame.getStatus().getI());
        result.setMaxPlayers(mvGame.getMaxPlayers());
        result.setNrOfPlayers(mvGame.getPlayers().size());
        result.setReqBuildings(mvGame.getReqBuildings());
        result.setTurnDuration(mvGame.getTurnDuration());
        result.setPlayerTurn(mvGame.getPlayerTurn());
        result.setCoinsLeft(mvGame.getCoinsLeft());
        result.setPlayersChosen(mvGame.isPlayersChosen());
        result.setCharacterTurn(mvGame.getCharacterTurn());
        return result;
    }

    public GameDto toLiteGameDto(MVGame mvGame) {
        GameDto result = new GameDto();
        result.setId(mvGame.getId());
        result.setGameName(mvGame.getName());
        result.setJoinType(mvGame.getJoinType().getI());
        result.setStatus(mvGame.getStatus().getI());
        result.setMaxPlayers(mvGame.getMaxPlayers());
        result.setNrOfPlayers(mvGame.getPlayers().size());
        result.setReqBuildings(mvGame.getReqBuildings());
        result.setTurnDuration(mvGame.getTurnDuration());
        return result;
    }

    public PlayerDto toDto(MVPlayer mvPlayer) {
        PlayerDto result = new PlayerDto();
        result.setId(mvPlayer.getId());
        BuildingCardDto[] buildingCardDtos = new BuildingCardDto[mvPlayer.getBuildingCards().size()];
        for (int i = 0; i < mvPlayer.getBuildingCards().size(); i++) {
            buildingCardDtos[i] = toDto(mvPlayer.getBuildingCards().get(i));
        }
        result.setBuildingCards(buildingCardDtos);
        if (mvPlayer.getUsers().size() > 0) {
            result.setUserName(mvPlayer.getUsers().get(0).getUsername());
        }
        if (mvPlayer.getCharacterCard() != null) {
            result.setCharacterCard(toDto(mvPlayer.getCharacterCard()));
        }
        result.setEndGamePoints(mvPlayer.getEndGamePoints());
        result.setHasCrown(mvPlayer.isHasCrown());
        result.setOrderNr(mvPlayer.getOrderNr());
        result.setNrOfCoins(mvPlayer.getNrOfCoins());
        result.setIsHost(mvPlayer.isHost());
        if (mvPlayer.getUsers().size() > 0) {
            result.setAvatar(mvPlayer.getUsers().get(0).getAvatar());
        }
        result.setFirstWithReqBuildings(mvPlayer.isFirstWithReqBuildings());
        return result;
    }

    public BuildingCardDto toDto(BuildingCard buildingCard) {
        BuildingCardDto result = new BuildingCardDto();
        result.setId(buildingCard.getId());
        result.setBuilding(this.toDto(buildingCard.getBuilding()));
        result.setBought(buildingCard.isBought());
        result.setDiscarded(buildingCard.isDiscarded());
        return result;
    }

    public BuildingDto toDto(MVBuilding mvBuilding) {
        BuildingDto result = new BuildingDto();
        result.setId(mvBuilding.getId());
        result.setBuildingName(mvBuilding.getBuildingName());
        result.setColor(mvBuilding.getColor().getI());
        result.setCost(mvBuilding.getCost());
        result.setImgPath(mvBuilding.getImgPath());
        return result;
    }

    public CharacterCardDto toDto(CharacterCard characterCard) {
        CharacterCardDto result = new CharacterCardDto();
        result.setId(characterCard.getId());
        result.setCharacter(toDto(characterCard.getCharacter()));
        result.setFaceUp(characterCard.isFaceUp());
        result.setKilled(characterCard.isKilled());
        result.setRobbed(characterCard.isRobbed());
        result.setPlacedMiddle(characterCard.isPlacedMiddle());
        return result;
    }

    public CharacterDto toDto(MVCharacter mvCharacter) {
        CharacterDto result = new CharacterDto();
        result.setId(mvCharacter.getId());
        result.setCharacterName(mvCharacter.getCharacterName());
        result.setExtraGoldColor(mvCharacter.getExtraGoldColor().getI());
        result.setImgPath(mvCharacter.getImgPath());
        return result;
    }

    public FriendDto toDto(Friend friend) {
        FriendDto result = new FriendDto();
        result.setId(friend.getId());
        result.setEmail(friend.getEmail());
        result.setUserName(friend.getUserName());
        return result;
    }

    public FriendRequestDto toDto(FriendRequest friendRequest) {
        FriendRequestDto result = new FriendRequestDto();
        result.setId(friendRequest.getId());
        result.setReceiver(friendRequest.getReceiver().getUsername());
        result.setSender(friendRequest.getSender().getUsername());
        result.setTimestamp(friendRequest.getTimestamp());
        return result;
    }

    public InviteDto toDto(Invite invite, MVGame game) {
        InviteDto result = new InviteDto();
        result.setId(invite.getId());
        result.setInvitedUserName(invite.getInvitedUser().getUsername());
        result.setInvitingUserName(invite.getInvitingUser().getUsername());
        result.setGameDto(toLiteGameDto(game));
        return result;
    }

    public UserDto toDto(User user) {
        UserDto result = new UserDto();
        result.setUsername(user.getUsername());
        result.setAvatar(user.getAvatar());
        result.setBirthday(user.getDateOfBirth().toString());
        result.setFirstName(user.getFirstName());
        result.setLastName(user.getLastName());
        result.setEmail(user.getEmail());
        return result;
    }


    public ChatMessageDto toDto(ChatMessage chatMessage, User user) {
        ChatMessageDto result = new ChatMessageDto();
        result.setAvatar(user.getAvatar());
        result.setContent(chatMessage.getMessage());
        result.setGame(toDto(chatMessage.getGame()));
        result.setTimestamp(chatMessage.getTimeStamp().toString());
        result.setUsername(user.getUsername());
        return result;
    }

    public CharacterCard fromDto(CharacterCard characterCard, CharacterCardDto dto) {
        characterCard.setId(dto.getId());
        characterCard.setFaceUp(dto.isFaceUp());
        characterCard.setKilled(dto.isKilled());
        characterCard.setRobbed(dto.isRobbed());
        characterCard.setPlacedMiddle(dto.isPlacedMiddle());
        return characterCard;
    }

    public BuildingCard fromDto(BuildingCard buildingCard, BuildingCardDto dto) {
        buildingCard.setId(dto.getId());
        buildingCard.setBought(dto.isBought());
        buildingCard.setDiscarded(dto.isDiscarded());
        return buildingCard;
    }
}
