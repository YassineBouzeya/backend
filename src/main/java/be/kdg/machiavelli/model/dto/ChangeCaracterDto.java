package be.kdg.machiavelli.model.dto;

public class ChangeCaracterDto {
    private int gameId;
    private int playerId;
    private int characterCardId;
    private GameDto gameDto;

    public ChangeCaracterDto(){}

    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    public int getPlayerId() {
        return playerId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    public int getCharacterCardId() {
        return characterCardId;
    }

    public void setCharacterCardId(int characterCardId) {
        this.characterCardId = characterCardId;
    }

    public GameDto getGameDto() {
        return gameDto;
    }

    public void setGameDto(GameDto gameDto) {
        this.gameDto = gameDto;
    }
}
