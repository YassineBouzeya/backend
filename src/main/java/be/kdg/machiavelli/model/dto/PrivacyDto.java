package be.kdg.machiavelli.model.dto;

public class PrivacyDto {
    private String username;
    private boolean isPublic;

    public PrivacyDto() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public void setPublic(boolean aPublic) {
        isPublic = aPublic;
    }
}
