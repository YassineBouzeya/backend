package be.kdg.machiavelli.model.game.logic;

public enum JoinType {
    OPEN(0), PRIVATE(1);

    private int i;
    JoinType(int i) {
        this.i=i;
    }

    public int getI() {
        return i;
    }
}
