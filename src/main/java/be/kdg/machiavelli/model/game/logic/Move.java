package be.kdg.machiavelli.model.game.logic;

import be.kdg.machiavelli.model.game.artifacts.BuildingCard;
import be.kdg.machiavelli.model.game.artifacts.CharacterCard;

import javax.persistence.*;
import java.util.List;

@Entity
public class Move {
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    private int id;
    private MoveType moveType;
    @OneToOne
    private CharacterCard characterCard;
    @OneToOne
    private MVPlayer player;
    @OneToOne
    private Round round;
    @ManyToMany(targetEntity = BuildingCard.class,
            cascade = {CascadeType.REMOVE})
    private List<BuildingCard> buildingCards;

    public Move(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public MoveType getMoveType() {
        return moveType;
    }

    public void setMoveType(MoveType moveType) {
        this.moveType = moveType;
    }

    public CharacterCard getCharacterCard() {
        return characterCard;
    }

    public void setCharacterCard(CharacterCard characterCard) {
        this.characterCard = characterCard;
    }

    public MVPlayer getPlayer() {
        return player;
    }

    public void setPlayer(MVPlayer player) {
        this.player = player;
    }

    public Round getRound() {
        return round;
    }

    public void setRound(Round round) {
        this.round = round;
    }

    public List<BuildingCard> getBuildingCards() {
        return buildingCards;
    }

    public void setBuildingCards(List<BuildingCard> buildingCards) {
        this.buildingCards = buildingCards;
    }
}
