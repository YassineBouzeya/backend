package be.kdg.machiavelli.model.game.logic;

import be.kdg.machiavelli.model.game.artifacts.MVCharacter;

import java.util.List;

import javax.persistence.*;
import java.util.List;

@Entity
public class MovePlayerInventory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @OneToOne
    private MVPlayer player;
    private int nrOfCoins;
    private boolean hasCrown;
    private boolean isKilled;
    private boolean isRobbed;
    @OneToOne
    private Move move;
    @OneToOne
    private MVCharacter character;

    @OneToMany(targetEntity = MovePlayerInventoryBuilding.class,
            cascade = {CascadeType.REMOVE})
    private List<MovePlayerInventoryBuilding> movePlayerInventoryBuildingList;

    public MovePlayerInventory() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public MVPlayer getPlayer() {
        return player;
    }

    public void setPlayer(MVPlayer player) {
        this.player = player;
    }

    public int getNrOfCoins() {
        return nrOfCoins;
    }

    public void setNrOfCoins(int nrOfCoins) {
        this.nrOfCoins = nrOfCoins;
    }

    public boolean isHasCrown() {
        return hasCrown;
    }

    public void setHasCrown(boolean hasCrown) {
        this.hasCrown = hasCrown;
    }

    public boolean isKilled() {
        return isKilled;
    }

    public void setKilled(boolean killed) {
        isKilled = killed;
    }

    public boolean isRobbed() {
        return isRobbed;
    }

    public void setRobbed(boolean robbed) {
        isRobbed = robbed;
    }

    public Move getMove() {
        return move;
    }

    public void setMove(Move move) {
        this.move = move;
    }

    public MVCharacter getCharacter() {
        return character;
    }

    public void setCharacter(MVCharacter character) {
        this.character = character;
    }

    public List<MovePlayerInventoryBuilding> getMovePlayerInventoryBuildingList() {
        return movePlayerInventoryBuildingList;
    }

    public void setMovePlayerInventoryBuildingList(List<MovePlayerInventoryBuilding> movePlayerInventoryBuildingList) {
        this.movePlayerInventoryBuildingList = movePlayerInventoryBuildingList;
    }
}
