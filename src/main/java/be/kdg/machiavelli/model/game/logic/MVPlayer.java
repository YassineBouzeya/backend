package be.kdg.machiavelli.model.game.logic;

import be.kdg.machiavelli.model.game.artifacts.BuildingCard;
import be.kdg.machiavelli.model.game.artifacts.CharacterCard;
import be.kdg.machiavelli.model.security.User;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class MVPlayer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @ManyToMany(targetEntity = User.class)
    private List<User> users;
    private String activeUserName;
    @OneToOne
    private CharacterCard characterCard;    //this changes per round

    @OneToMany(targetEntity = BuildingCard.class,
            cascade = {CascadeType.REMOVE})
    private List<BuildingCard> buildingCards;
    private boolean hasCrown;               //gets determined during the round
    private int orderNr;                    //gets determined where the player is sitting in the table
    private int nrOfCoins;
    private int endGamePoints;
    private boolean isHost;
    private boolean isTurn;
    private boolean firstWithReqBuildings;


    public MVPlayer() {
    }

    public MVPlayer(User user, int orderNr, boolean isHost) {
        this.users = new ArrayList<>();
        users.add(user);
        this.characterCard = null;
        this.buildingCards = new ArrayList<>();
        this.hasCrown = false;
        this.orderNr = orderNr;
        this.nrOfCoins = 0;
        this.endGamePoints = 0;
        this.isHost = isHost;
    }

    public boolean isTurn() {
        return isTurn;
    }

    public void setTurn(boolean turn) {
        isTurn = turn;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public CharacterCard getCharacterCard() {
        return characterCard;
    }

    public void setCharacterCard(CharacterCard characterCard) {
        this.characterCard = characterCard;
    }

    public List<BuildingCard> getBuildingCards() {
        return buildingCards;
    }

    public void setBuildingCards(List<BuildingCard> buildingCards) {
        this.buildingCards = buildingCards;
    }

    public boolean isHasCrown() {
        return hasCrown;
    }

    public void setHasCrown(boolean hasCrown) {
        this.hasCrown = hasCrown;
    }

    public int getOrderNr() {
        return orderNr;
    }

    public void setOrderNr(int orderNr) {
        this.orderNr = orderNr;
    }

    public int getNrOfCoins() {
        return nrOfCoins;
    }

    public void setNrOfCoins(int nrOfCoins) {
        this.nrOfCoins = nrOfCoins;
    }

    public int getEndGamePoints() {
        return endGamePoints;
    }

    public void setEndGamePoints(int endGamePoints) {
        this.endGamePoints = endGamePoints;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isHost() {
        return isHost;
    }

    public void setHost(boolean host) {
        isHost = host;
    }

    public String getActiveUserName() {
        return activeUserName;
    }

    public void setActiveUserName(String activeUserName) {
        this.activeUserName = activeUserName;
    }

    public boolean isFirstWithReqBuildings() {
        return firstWithReqBuildings;
    }

    public void setFirstWithReqBuildings(boolean firstWithReqBuildings) {
        this.firstWithReqBuildings = firstWithReqBuildings;
    }
}
