package be.kdg.machiavelli.model.game.artifacts;

import be.kdg.machiavelli.model.game.logic.Color;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class MVBuilding {
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    private int id;
    private String buildingName;
    private String imgPath;
    private Color color;
    private int cost;
    private int cardAmount;


    public MVBuilding(){}

    public MVBuilding(String buildingName, String imgPath, Color color, int cost, int cardAmount) {
        this.buildingName = buildingName;
        this.imgPath = imgPath;
        this.color = color;
        this.cost = cost;
        this.cardAmount=cardAmount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }
    public int getCardAmount() {
        return cardAmount;
    }

    public void setCardAmount(int cardAmount) {
        this.cardAmount = cardAmount;
    }
}
