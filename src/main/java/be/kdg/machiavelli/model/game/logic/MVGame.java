package be.kdg.machiavelli.model.game.logic;

import be.kdg.machiavelli.model.game.artifacts.BuildingCard;
import be.kdg.machiavelli.model.game.artifacts.CharacterCard;
import be.kdg.machiavelli.model.game.artifacts.MVCharacter;
import be.kdg.machiavelli.model.user.ChatMessage;
import be.kdg.machiavelli.model.user.Notification;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class MVGame {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private GameStatus status;
    private String name;
    private int coinsLeft;
    private int turnDuration;
    private int reqBuildings;
    private int maxPlayers;
    private JoinType joinType;
    private int playerTurn;
    private int characterTurn;
    private boolean playersChosen;
    @OneToMany(targetEntity = Round.class,
            cascade = {CascadeType.REMOVE})
    private List<Round> rounds;                         //Will be used for the Replay
    @OneToMany(targetEntity = MVPlayer.class,
            cascade = {CascadeType.REMOVE})
    private List<MVPlayer> players;                     //These are the participating players
    @OneToMany(targetEntity = BuildingCard.class,
            cascade = {CascadeType.REMOVE})
    private List<BuildingCard> buildings;               //This is the stack of cards in the middle
    @OneToMany(targetEntity = CharacterCard.class,
            cascade = {CascadeType.REMOVE})
    private List<CharacterCard> characters;               //These are the characters of the game
    @OneToMany(targetEntity = ChatMessage.class,
            cascade = {CascadeType.REMOVE})
    private List<ChatMessage> chatMessages;
    @OneToMany(targetEntity = Notification.class,
            cascade = {CascadeType.REMOVE})
    private List<Notification> notifications;

    public MVGame() {
        this.players = new ArrayList<>();
        this.rounds = new ArrayList<>();
        this.characters = new ArrayList<>();
        this.buildings = new ArrayList<>();
        this.chatMessages = new ArrayList<>();
    }

    public MVGame(String name, GameStatus status, int turnDuration, int reqBuildings, int maxPlayers, JoinType joinType) {
        this.status = status;
        this.joinType = joinType;
        this.coinsLeft = 30;
        this.playerTurn = 0;
        this.players = new ArrayList<>();
        this.rounds = new ArrayList<>();
        this.buildings = new ArrayList<>();
        this.characters = new ArrayList<>();
        this.chatMessages = new ArrayList<>();
        this.turnDuration = turnDuration;
        this.reqBuildings = reqBuildings;
        this.maxPlayers = maxPlayers;
        this.name = name;
    }


    public JoinType getJoinType() {
        return joinType;
    }

    public void setJoinType(JoinType joinType) {
        this.joinType = joinType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public GameStatus getStatus() {
        return status;
    }

    public void setStatus(GameStatus status) {
        this.status = status;
    }

    public int getCoinsLeft() {
        return coinsLeft;
    }

    public void setCoinsLeft(int coinsLeft) {
        this.coinsLeft = coinsLeft;
    }

    public List<Round> getRounds() {
        return rounds;
    }

    public void setRounds(List<Round> rounds) {
        this.rounds = rounds;
    }

    public List<MVPlayer> getPlayers() {
        return players;
    }

    public void setPlayers(List<MVPlayer> players) {
        this.players = players;
    }

    public List<BuildingCard> getBuildings() {
        return buildings;
    }

    public void setBuildings(List<BuildingCard> buildings) {
        this.buildings = buildings;
    }

    public List<CharacterCard> getCharacters() {
        return characters;
    }

    public void setCharacters(List<CharacterCard> characters) {
        this.characters = characters;
    }

    public List<ChatMessage> getChatMessages() {
        return chatMessages;
    }

    public void setChatMessages(List<ChatMessage> chatMessages) {
        this.chatMessages = chatMessages;
    }

    public List<Notification> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<Notification> notifications) {
        this.notifications = notifications;
    }

    public int getTurnDuration() {
        return turnDuration;
    }

    public void setTurnDuration(int turnDuration) {
        this.turnDuration = turnDuration;
    }

    public int getReqBuildings() {
        return reqBuildings;
    }

    public void setReqBuildings(int reqBuildings) {
        this.reqBuildings = reqBuildings;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public void setMaxPlayers(int maxPlayers) {
        this.maxPlayers = maxPlayers;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPlayerTurn() {
        return playerTurn;
    }

    public void setPlayerTurn(int playerTurn) {
        this.playerTurn = playerTurn;
    }

    public int getCharacterTurn() {
        return characterTurn;
    }

    public void setCharacterTurn(int characterTurn) {
        this.characterTurn = characterTurn;
    }

    public boolean isPlayersChosen() {
        return playersChosen;
    }

    public void setPlayersChosen(boolean playersChosen) {
        this.playersChosen = playersChosen;
    }
}
