package be.kdg.machiavelli.model.game.artifacts;

import be.kdg.machiavelli.model.game.logic.Color;
import be.kdg.machiavelli.model.game.logic.MVPlayer;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class MVCharacter {
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    private int id;
    private String characterName;
    private String imgPath;
    private Color extraGoldColor;

    public MVCharacter(){}

    public MVCharacter(String characterName, String imgPath, Color extraGoldColor) {
        this.characterName = characterName;
        this.imgPath = imgPath;
        this.extraGoldColor = extraGoldColor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCharacterName() {
        return characterName;
    }

    public void setCharacterName(String characterName) {
        this.characterName = characterName;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public Color getExtraGoldColor() {
        return extraGoldColor;
    }

    public void setExtraGoldColor(Color extraGoldColor) {
        this.extraGoldColor = extraGoldColor;
    }
}
