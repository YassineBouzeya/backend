package be.kdg.machiavelli.model.game.artifacts;

import javax.persistence.*;

@Entity
public class BuildingCard {
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    private int id;
    @OneToOne
    private MVBuilding building;
    private boolean isBought;
    private boolean isDiscarded;

    public BuildingCard (){}

    public BuildingCard(MVBuilding building) {
        this.building = building;
        this.isBought = false;
        this.isDiscarded = false;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public MVBuilding getBuilding() {
        return building;
    }

    public void setBuilding(MVBuilding building) {
        this.building = building;
    }

    public boolean isBought() {
        return isBought;
    }

    public void setBought(boolean bought) {
        isBought = bought;
    }

    public boolean isDiscarded() {
        return isDiscarded;
    }

    public void setDiscarded(boolean discarded) {
        isDiscarded = discarded;
    }
}
