package be.kdg.machiavelli.model.user;

import be.kdg.machiavelli.model.game.logic.MVGame;
import be.kdg.machiavelli.model.security.User;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class Replay {
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    private int id;
    @OneToOne
    private MVGame game;
    @OneToOne
    private User user;
    private LocalDateTime expirationDate;

    public Replay(){
        this.expirationDate = LocalDateTime.now().plusDays(7);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public MVGame getGame() {
        return game;
    }

    public void setGame(MVGame game) {
        this.game = game;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LocalDateTime getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(LocalDateTime expirationDate) {
        this.expirationDate = expirationDate;
    }
}
