package be.kdg.machiavelli.model.user;

import be.kdg.machiavelli.model.game.logic.MVGame;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class ChatMessage {
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    private int id;
    private String message;
    private LocalDateTime timeStamp;

    @OneToOne
    private MVGame game;

    public ChatMessage(String message, MVGame game) {
        this.message = message;
        this.timeStamp = LocalDateTime.now();
        this.game=game;
    }

    public ChatMessage(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(LocalDateTime timeStamp) {
        this.timeStamp = timeStamp;
    }

    public MVGame getGame() {
        return game;
    }

    public void setGame(MVGame game) {
        this.game = game;
    }
}
