package be.kdg.machiavelli.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

import java.util.Arrays;

@Configuration
@EnableAuthorizationServer      //Enables an authorization server
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {
    //AuthorizationServerConfig which is our authorization server configuration class extends AuthorizationServerConfigurerAdapter
    // which in turn is an implementation of AuthorizationServerConfigurer.
    //The presence of a bean of type AuthorizationServerConfigurer simply
    // tells Spring Boot to switch off auto-configuration and use the custom configuration.
    @Value("${security.jwt.client-id}")
    private String clientId; //defines the id of the client application that is authorized to authenticate,
    // the client application provides this in order to be allowed to send request to the server

    @Value("${security.jwt.client-secret}")
    private String clientSecret; //is the client application’s password.

    @Value("${security.jwt.grant-type}")
    private String grantType;   //we define grant type password here because it’s not enabled by default

    @Value("${security.jwt.scope-read}")
    private String scopeRead;   //read, write defines the level of access we are allowing to resources

    @Value("${security.jwt.scope-write}")
    private String scopeWrite = "write";

    @Value("${security.jwt.resource-ids}")
    private String resourceIds;     //The resource Id specified here must be specified on the resource server as well

    @Autowired
    private TokenStore tokenStore;

    @Autowired
    private JwtAccessTokenConverter accessTokenConverter;

    @Autowired
    private AuthenticationManager authenticationManager;        //Spring’s authentication manager takes care checking user credential validity

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void configure(ClientDetailsServiceConfigurer configurer) throws Exception {
        configurer
                .inMemory()
                .withClient(clientId)
                // clientSecret mag zeker niet exposed worde.... daarom encoden we da
                .secret(passwordEncoder.encode(clientSecret))
                .authorizedGrantTypes(grantType)
                .scopes(scopeRead, scopeWrite)
                .resourceIds(resourceIds);
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        TokenEnhancerChain enhancerChain = new TokenEnhancerChain();        //We define a token enhancer that enables chaining multiple types of claims containing different information
        enhancerChain.setTokenEnhancers(Arrays.asList(accessTokenConverter));
        endpoints.tokenStore(tokenStore)
                .accessTokenConverter(accessTokenConverter)
                .tokenEnhancer(enhancerChain)
                .authenticationManager(authenticationManager);

    }
}