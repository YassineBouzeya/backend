package be.kdg.machiavelli;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@EntityScan(basePackages = {"be.kdg.machiavelli.model"})
public class MachiavelliApplication {

    public static void main(String[] args) {
        SpringApplication.run(MachiavelliApplication.class, args);
    }

}

