package be.kdg.machiavelli.services;

import be.kdg.machiavelli.model.dto.DtoMapper;
import be.kdg.machiavelli.model.dto.FriendDto;
import be.kdg.machiavelli.model.dto.FriendRequestDto;
import be.kdg.machiavelli.model.security.User;
import be.kdg.machiavelli.model.user.Friend;
import be.kdg.machiavelli.model.user.FriendRequest;
import be.kdg.machiavelli.repositories.FriendRepo;
import be.kdg.machiavelli.repositories.FriendRequestRepo;
import javassist.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class FriendService {
    private static final Logger LOGGER = LoggerFactory.getLogger(FriendService.class);

    private FriendRepo friendRepo;
    private FriendRequestRepo friendRequestRepo;
    private UserService userService;
    private DtoMapper dtoMapper;

    public FriendService(FriendRepo friendRepo, FriendRequestRepo friendRequestRepo, UserService userService, DtoMapper dtoMapper) {
        this.friendRepo = friendRepo;
        this.friendRequestRepo = friendRequestRepo;
        this.userService = userService;
        this.dtoMapper = dtoMapper;
    }

    /**
     * Method that will return a list of received friend requests
     * @param username
     * @return
     */
    public List<FriendRequestDto> getReceivedFriendRequests(String username) {
        User user = userService.findByUsername(username);
        List<FriendRequestDto> friendRequestDtos = new ArrayList<>();
        for (FriendRequest fr : this.friendRequestRepo.findAllByReceiver(user)) {
            friendRequestDtos.add(this.dtoMapper.toDto(fr));
        }
        return friendRequestDtos;
    }

    /**
     * This method allows you to send a friend request
     * @param userName
     * @param friendName
     * @return
     */
    public FriendRequestDto sendFriendRequest(String userName, String friendName) {
        try {
            if (userName.equals(friendName)) return null;
            List<Friend> friends = this.userService.getFriends(userName);
            //friends.contains(friendName); ???
            if (friends.stream().anyMatch(f -> f.getUserName().equals(friendName))) {
                return null;
            }
            User sender = this.userService.findByUsername(userName);
            User receiver = this.userService.findByUsername(friendName);
            if (receiver != null) {
                FriendRequest existingRequest = this.friendRequestRepo.findBySenderAndReceiver(sender, receiver);
                if (existingRequest == null) {
                    existingRequest = this.friendRequestRepo.findBySenderAndReceiver(receiver, sender);
                }
                if (existingRequest == null) {
                    FriendRequest friendRequest = new FriendRequest(sender, receiver);
                    this.friendRequestRepo.save(friendRequest);
                    return this.dtoMapper.toDto(friendRequest);
                } else return null;
            }
        }catch (EntityNotFoundException e){
            LOGGER.error("friend was not found - " + e.getMessage());

        }

        return null;
    }

    /***
     * Method that returns the friend requests that you sent
     * @param username
     * @return
     */
    public List<FriendRequestDto> getSentFriendRequests(String username) {
        User user = userService.findByUsername(username);
        List<FriendRequestDto> friendRequestDtos = new ArrayList<>();
        for (FriendRequest fr : this.friendRequestRepo.findAllBySender(user)) {
            friendRequestDtos.add(this.dtoMapper.toDto(fr));
        }
        return friendRequestDtos;
    }

    /***
     * Method that makes it possible to accept a recieved friend request
     * @param name
     * @param friendName
     * @return
     */
    public FriendDto acceptRequest(String name, String friendName) {
        User acceptingUser = userService.findByUsername(name);
        User acceptedUser = userService.findByUsername(friendName);
        this.friendRequestRepo.delete(this.friendRequestRepo.findBySenderAndReceiver(acceptedUser, acceptingUser));
        Friend acceptingFriend = friendRepo.findByUserName(name);
        Friend acceptedFriend = friendRepo.findByUserName(friendName);
        acceptingUser.getFriends().add(acceptedFriend);
        acceptedUser.getFriends().add(acceptingFriend);
        userService.saveUser(acceptingUser);
        userService.saveUser(acceptedUser);
        return dtoMapper.toDto(acceptedFriend);
    }

    public void save(Friend friend) {
        friendRepo.save(friend);
    }

    public FriendDto getFriendByUsername(String username) {
        return this.dtoMapper.toDto(this.friendRepo.findByUserName(username));
    }

    /***
     * Method that mekes it possible to remove a friend that you have added previously
     * @param username
     * @param friendToRemoveName
     * @return
     */
    public FriendDto removeFriend(String username, String friendToRemoveName) {
        User deletingUser = this.userService.findByUsername(username);
        User userToDelete = this.userService.findByUsername(friendToRemoveName);
        Friend deletingFriend = this.friendRepo.findByUserName(username);
        Friend friendToDelete = this.friendRepo.findByUserName(friendToRemoveName);
        deletingUser.getFriends().remove(friendToDelete);
        userToDelete.getFriends().remove(deletingFriend);
        return this.dtoMapper.toDto(deletingFriend);
    }

    /***
     * method that makes it possible to deny a received friend request
     * @param username
     * @param deniedFriendName
     * @return
     */
    public FriendDto denyRequest(String username, String deniedFriendName) {
        User denyingUser = this.userService.findByUsername(username);
        User deniedUser = this.userService.findByUsername(deniedFriendName);
        this.friendRequestRepo.delete(this.friendRequestRepo.findBySenderAndReceiver(deniedUser, denyingUser));
        return dtoMapper.toDto(friendRepo.findByUserName(username));
    }

    /***
     * method that makes it possible to remove a received friend request
     * @param username
     * @param removedFriendName
     * @return
     */
    public FriendDto removeRequest(String username, String removedFriendName) {
        User requestRemovedUser = this.userService.findByUsername(removedFriendName);
        User requestRemovingUser = this.userService.findByUsername(username);
        FriendRequest friendRequest = this.friendRequestRepo.findBySenderAndReceiver(requestRemovingUser, requestRemovedUser);
        this.friendRequestRepo.delete(friendRequest);
        return dtoMapper.toDto(friendRepo.findByUserName(username));
    }

}
