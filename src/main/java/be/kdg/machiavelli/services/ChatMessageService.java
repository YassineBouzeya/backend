package be.kdg.machiavelli.services;

import be.kdg.machiavelli.model.dto.ChatMessageDto;
import be.kdg.machiavelli.model.dto.DtoMapper;
import be.kdg.machiavelli.model.game.logic.MVGame;
import be.kdg.machiavelli.model.security.User;
import be.kdg.machiavelli.model.user.ChatMessage;
import be.kdg.machiavelli.repositories.ChatMessageRepo;
import org.springframework.stereotype.Service;

@Service

public class ChatMessageService {
    private UserService userService;
    private GameService gameService;
    private DtoMapper dtoMapper;
    private ChatMessageRepo chatMessageRepo;
    public ChatMessageService(UserService userService, GameService gameService, DtoMapper dtoMapper, ChatMessageRepo chatMessageRepo) {
        this.userService = userService;
        this.gameService = gameService;
        this.dtoMapper = dtoMapper;
        this.chatMessageRepo = chatMessageRepo;
    }

    public ChatMessageDto sendMessage(String username, ChatMessageDto message) {
        User user = this.userService.findByUsername(username);
        MVGame game = this.gameService.getGame(message.getGame().getId());
        ChatMessage newMessage = new ChatMessage(message.getContent(),game);
        newMessage= this.chatMessageRepo.save(newMessage);
        user.getChatMessages().add(newMessage);
        userService.saveUser(user);
        return dtoMapper.toDto(newMessage,user);
    }
}
