package be.kdg.machiavelli.services;

import be.kdg.machiavelli.model.dto.DtoMapper;
import be.kdg.machiavelli.model.dto.InviteDto;
import be.kdg.machiavelli.model.game.logic.GameStatus;
import be.kdg.machiavelli.model.game.logic.MVGame;
import be.kdg.machiavelli.model.security.User;
import be.kdg.machiavelli.model.user.Invite;
import be.kdg.machiavelli.repositories.InviteRepo;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class InviteService {
    private UserService userService;
    private GameService gameService;
    private InviteRepo inviteRepo;
    private DtoMapper dtoMapper;

    public InviteService(UserService userService, GameService gameService, InviteRepo inviteRepo, DtoMapper dtoMapper) {
        this.userService = userService;
        this.gameService = gameService;
        this.inviteRepo = inviteRepo;
        this.dtoMapper = dtoMapper;
    }

    /***
     *Method that will initiate and persist invites
     * @param name
     * @param friendName
     * @param gameId
     * @return
     */
    public InviteDto invite(String name, String friendName, int gameId) {
        User invitingUser = userService.findByUsername(name);
        User userThatGetsInvited = userService.findByUsername(friendName);
        MVGame game = gameService.getGame(gameId);
        if (userThatGetsInvited == null) {
            throw new UsernameNotFoundException("User" + friendName + "");
        }
        Invite invite = new Invite(invitingUser, userThatGetsInvited, game);
        invite = inviteRepo.save(invite);
        return dtoMapper.toDto(invite, game);
    }

    /***
     * Method that will return a list of invites of a particular user
     * @param name
     * @return
     */
    public List<InviteDto> getInvites(String name) {
        List<InviteDto> inviteDtos = new ArrayList<>();
        User user = this.userService.findByUsername(name);
        for (Invite i : this.inviteRepo.findAllByInvitedUser(user)) {
            MVGame g = gameService.getGame(i.getGame().getId());
            if (g.getStatus() == GameStatus.LOBBY) {
                inviteDtos.add(dtoMapper.toDto(i, g));
            }
        }
        return inviteDtos;
    }

    /***
     * Method that removes a received or sent invite
     * @param id
     */
    public void removeInvite(int id) {
        Invite invite = this.inviteRepo.getOne(id);
        this.inviteRepo.delete(invite);
    }
}
