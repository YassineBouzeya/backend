package be.kdg.machiavelli.services;

import be.kdg.machiavelli.model.dto.*;
import be.kdg.machiavelli.model.security.Role;
import be.kdg.machiavelli.model.security.User;
import be.kdg.machiavelli.model.user.Friend;
import be.kdg.machiavelli.repositories.RoleRepository;
import be.kdg.machiavelli.repositories.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@Service
@Transactional
public class UserService implements UserDetailsService {

    private UserRepo userRepo;

    private BCryptPasswordEncoder encoder;

    private RoleRepository roleRepository;

    private DtoMapper dtoMapper;

    @Autowired
    private FriendService friendService;

    public UserService(UserRepo userRepo, BCryptPasswordEncoder encoder, RoleRepository roleRepository, DtoMapper dtoMapper) {
        this.userRepo = userRepo;
        this.encoder = encoder;
        this.roleRepository = roleRepository;
        this.dtoMapper = dtoMapper;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userRepo.findByUsername(s);

        if (user == null) {
            throw new UsernameNotFoundException(String.format("The username %s doesn't exist", s));
        }

        List<GrantedAuthority> authorities = new ArrayList<>();
        user.getRoles().forEach(role -> {
            authorities.add(new SimpleGrantedAuthority(role.getRoleName()));
        });

        UserDetails userDetails = new org.springframework.security.core.userdetails.
                User(user.getUsername(), user.getPassword(), authorities);

        return userDetails;
    }

    public User saveUser(User user) {
        return this.userRepo.save(user);
    }

    public User createNewUser(UserDto user) {
        Friend friend = new Friend(user.getUsername(), user.getEmail());
        friendService.save(friend);
        return this.userRepo.save(convertDto(user));
    }

    public User findByUsername(String username) {
        return userRepo.findByUsername(username);
    }

    private User convertDto(UserDto userDto) {
        userDto.setPassword(encoder.encode(userDto.getPassword()));
        User user = new User();
        String[] splitDate = userDto.getBirthday().split("-");
        int year = Integer.parseInt(splitDate[0]);
        int month = Integer.parseInt(splitDate[1]);
        int day = Integer.parseInt(splitDate[2]);
        LocalDateTime date = LocalDateTime.of(year, month, day, 0, 0);

        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setUsername(userDto.getUsername());
        user.setPassword(userDto.getPassword());
        user.setAverageScore(0);
        user.setEmail(userDto.getEmail());
        user.setNumberOfGames(0);
        user.setNumberOfWins(0);
        user.setTopScore(0);
        user.setReplayLiveDays(7);
        user.setDateOfBirth(date);       //need to fix it
        user.setFriendRequestNotifications(true);
        user.setInviteNotifications(true);
        user.setOtherTurnNotifications(true);
        user.setMyTurnNotifications(true);
        user.setTurnIsExpiringNotifications(true);
        user.setChatNotifications(true);
        user.setPublic(true);
        Role role = roleRepository.getOne((long) 1);     //Simple user
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(role);
        user.setRoles(roles);
        user.setAvatar("avatar1");
        return user;
    }

    public boolean userNameBusy(String username) {
        User userWithUserName = userRepo.findByUsername(username);

        if (userWithUserName != null) {
            return true;
        } else {
            return false;
        }
    }

    public boolean emailBusy(String email) {
        User userWithEmail = userRepo.findByEmail(email);
        if (userWithEmail != null) {
            return true;
        } else {
            return false;
        }
    }

    public List<FriendDto> getFriendDtos(String username) {
        List<FriendDto> friendDtos = new ArrayList<>();
        for (Friend f : this.userRepo.findByUsername(username).getFriends()) {
            friendDtos.add(dtoMapper.toDto(f));
        }
        return friendDtos;
    }

    public List<Friend> getFriends(String username) {
        return this.userRepo.findByUsername(username).getFriends();
    }

    public List<User> findAllUsers() {
        return (List<User>) this.userRepo.findAll();
    }

    public StatisticsDto getStatisticsDto(String username) {
        StatisticsDto statisticsDto = new StatisticsDto();
        User user = this.userRepo.findByUsername(username);

        statisticsDto.setNumberOfGames(user.getNumberOfGames());
        statisticsDto.setNumberOfWins(user.getNumberOfWins());
        statisticsDto.setAverageScore(user.getAverageScore());
        statisticsDto.setTopScore(user.getTopScore());

        return statisticsDto;
    }

    public UserDto getUser(String name) {
        User user = this.userRepo.findByUsername(name);
        return this.dtoMapper.toDto(user);
    }

    public List<SearchUsersDto> getUsersContainingUsername(String username) {
        List<SearchUsersDto> searchUsersDtoList = new ArrayList<>();
        List<User> foundUsers = this.userRepo.findAllByUsernameContains(username);

        for (User user : foundUsers) {
            SearchUsersDto searchUsersDto = new SearchUsersDto();
            searchUsersDto.setUsername(user.getUsername());
            searchUsersDtoList.add(searchUsersDto);
        }

        return searchUsersDtoList;
    }

    public String saveUserWithDto(UserDto userDto) {
        User existingUser = findByUsername(userDto.getUsername());
        existingUser.setFirstName(userDto.getFirstName());
        existingUser.setLastName(userDto.getLastName());
        existingUser.setEmail(userDto.getEmail());
        if (userDto.getNewUsername() == null) {
            existingUser.setUsername(userDto.getUsername());
        } else {
            existingUser.setUsername(userDto.getNewUsername());
        }
        //existingUser.setPassword(encoder.encode(userDto.getPassword()));
        existingUser.setAvatar(userDto.getAvatar());

        this.userRepo.save(existingUser);

        return "User saved";
    }

    public String saveDeviceId(DeviceDto deviceDto) {
        User user = findByUsername(deviceDto.getUsername());
        user.setDeviceId(deviceDto.getDeviceId());

        this.userRepo.save(user);

        return "Device Id saved";
    }

    @SuppressWarnings("Duplicates")
    public String saveSettings(SettingsDto settingsDto) {
        User user = findByUsername(settingsDto.getUsername());
        user.setFriendRequestNotifications(settingsDto.isFriendRequestNotifications());
        user.setInviteNotifications(settingsDto.isInviteNotifications());
        user.setOtherTurnNotifications(settingsDto.isOtherTurnNotifications());
        user.setMyTurnNotifications(settingsDto.isMyTurnNotifications());
        user.setTurnIsExpiringNotifications(settingsDto.isTurnIsExpiringNotifications());
        user.setChatNotifications(settingsDto.isChatNotifications());
        user.setPublic(settingsDto.isPublic());

        this.userRepo.save(user);

        return "Settings saved";
    }

    @SuppressWarnings("Duplicates")
    public SettingsDto getSettings(String username) {
        SettingsDto settingsDto = new SettingsDto();
        User user = findByUsername(username);
        settingsDto.setUsername(user.getUsername());
        settingsDto.setFriendRequestNotifications(user.isFriendRequestNotifications());
        settingsDto.setInviteNotifications(user.isInviteNotifications());
        settingsDto.setOtherTurnNotifications(user.isOtherTurnNotifications());
        settingsDto.setMyTurnNotifications(user.isMyTurnNotifications());
        settingsDto.setTurnIsExpiringNotifications(user.isTurnIsExpiringNotifications());
        settingsDto.setChatNotifications(user.isChatNotifications());
        settingsDto.setPublic(user.isPublic());

        return settingsDto;
    }

    public PrivacyDto getPrivacySettings(String username) {
        PrivacyDto privacyDto = new PrivacyDto();
        User user = findByUsername(username);

        privacyDto.setUsername(user.getUsername());
        privacyDto.setPublic(user.isPublic());

        return privacyDto;
    }
}
