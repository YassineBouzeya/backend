package be.kdg.machiavelli.services.securityImpl;


import be.kdg.machiavelli.model.security.User;
import be.kdg.machiavelli.repositories.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class GenericServiceImpl implements GenericService {
    @Autowired
    private UserRepo userRepo;

    @Override
    public User findByUsername(String username) {
        return userRepo.findByUsername(username);
    }

    @Override
    public List<User> findAllUsers() {
        return (List<User>) userRepo.findAll();
    }


}
