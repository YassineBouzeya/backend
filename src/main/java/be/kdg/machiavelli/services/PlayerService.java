package be.kdg.machiavelli.services;

import be.kdg.machiavelli.model.game.logic.MVPlayer;
import be.kdg.machiavelli.model.security.User;
import be.kdg.machiavelli.repositories.MVPlayerRepo;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class PlayerService {
    @Autowired
    private MVPlayerRepo mvPlayerRepo;
    @Autowired
    private GameService gameService;

    public MVPlayer findOnePlayerById(int id) {
        return this.mvPlayerRepo.getOne(id);
    }

    public MVPlayer save(MVPlayer mvPlayer) {
        return this.mvPlayerRepo.save(mvPlayer);
    }

    public List<MVPlayer> findAllByUsersContains(User user) {
        return this.mvPlayerRepo.findAllByUsersContains(user);
    }

    public List<MVPlayer> getPlayersFromGame(int gameId) {
        return gameService.getGame(gameId).getPlayers();
    }
}
