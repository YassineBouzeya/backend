package be.kdg.machiavelli.services;

import be.kdg.machiavelli.model.dto.DtoMapper;
import be.kdg.machiavelli.model.dto.GameDto;
import be.kdg.machiavelli.model.dto.PlayerDto;
import be.kdg.machiavelli.model.dto.UserDto;
import be.kdg.machiavelli.model.game.artifacts.BuildingCard;
import be.kdg.machiavelli.model.game.artifacts.CharacterCard;
import be.kdg.machiavelli.model.game.artifacts.MVBuilding;
import be.kdg.machiavelli.model.game.artifacts.MVCharacter;
import be.kdg.machiavelli.model.game.logic.*;
import be.kdg.machiavelli.model.security.User;
import be.kdg.machiavelli.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class LobbyService {
    private final MVGameRepo gameRepo;
    private final UserService userService;
    private final PlayerService playerService;
    private final GameService gameService;
    private final CharacterService characterService;
    private final CharacterCardService characterCardService;
    private final BuildingService buildingService;
    private final BuildingCardService buildingCardService;


    private DtoMapper dtoMapper;

    @Autowired
    public LobbyService(MVGameRepo gameRepo, UserService userService, PlayerService playerService, GameService gameService, CharacterService characterService, CharacterCardService characterCardService, BuildingService buildingService, BuildingCardService buildingCardService, DtoMapper dtoMapper) {
        this.gameRepo = gameRepo;
        this.userService = userService;
        this.playerService = playerService;
        this.gameService = gameService;
        this.characterService = characterService;
        this.characterCardService = characterCardService;
        this.buildingService = buildingService;
        this.buildingCardService = buildingCardService;
        this.dtoMapper = dtoMapper;
    }


    /***
     * This method makes the lobby that will be used to make different players join and start the game
     * @param gameDTO (object with all the parameters to make a game)
     * @return MVGame game object
     */
    public GameDto makeLobby(GameDto gameDTO) {
        //create new game
        JoinType gameJoinType = null;
        if (gameDTO.getJoinType() == 0) {
            gameJoinType = JoinType.OPEN;
        }
        if (gameDTO.getJoinType() == 1) {
            gameJoinType = JoinType.PRIVATE;
        }
        MVGame newGame = new MVGame(gameDTO.getGameName(), GameStatus.LOBBY, gameDTO.getTurnDuration(), gameDTO.getReqBuildings(), gameDTO.getMaxPlayers(), gameJoinType);
        //update objects and insert to db

        newGame = gameRepo.save(newGame);
        return dtoMapper.toDto(newGame);
    }

    /***
     * makes it possible to join a lobby
     * @param userName
     * @param gameId
     * @return list of players
     */
    public ArrayList<PlayerDto> joinLobby(String userName, int gameId) {
        //boolean isJoined = false;
        boolean isHost = false;
        ArrayList<PlayerDto> playerDtos = new ArrayList<>();
        User user = this.userService.findByUsername(userName);
        MVGame game = this.gameRepo.findById(gameId);

        if (game.getPlayers() != null) {
            for (MVPlayer p : game.getPlayers()) {
                playerDtos.add(dtoMapper.toDto(p));
            }
        }
        if (IsAlreadyJoined(user, game)) {
            return playerDtos;
        }
        if (playerDtos.isEmpty()) {
            isHost = true;
        }
        MVPlayer joiningPlayer = new MVPlayer(user, game.getPlayers().size() + 1, isHost);

        joiningPlayer.setOrderNr(game.getPlayers().size());
        joiningPlayer.setActiveUserName(userName);
        playerDtos.add(dtoMapper.toDto(joiningPlayer));
        game.getPlayers().add(joiningPlayer);
        this.playerService.save(joiningPlayer);
        user.getPlayers().add(joiningPlayer);
        this.userService.saveUser(user);
        this.gameRepo.save(game);
        return playerDtos;
    }

    /***
     * check if a user is already joined
     * @param user
     * @param game
     * @return
     */
    public Boolean IsAlreadyJoined(User user, MVGame game) {
        if (game.getPlayers() != null) {
            for (MVPlayer p : game.getPlayers()) {
                if (p.getUsers().contains(user)) {
                    return true;
                }
            }
        }
        return false;
    }

    /***
     * create the card decks that will be used
     * @return a list of buildingcards
     */
    public List<BuildingCard> createBuildingCards() {
        List<MVBuilding> buildings = this.buildingService.findAll();
        List<BuildingCard> deckBuildingCards = new ArrayList<>();
        for (MVBuilding b : buildings) {
            for (int i = 0; i <= b.getCardAmount(); i++) {
                BuildingCard buildingCard = new BuildingCard(b);
                deckBuildingCards.add(buildingCard);
            }
        }
        return deckBuildingCards;
    }

    /***
     *  create the card decks that will be used
     * @return a list of CharacterCards
     */
    public List<CharacterCard> createCharacterCards() {
        List<MVCharacter> characters = this.characterService.findAll();
        List<CharacterCard> deckCharacterCards = new ArrayList<>();
        for (MVCharacter c : characters) {
            CharacterCard charactercard = new CharacterCard(c);
            deckCharacterCards.add(charactercard);
        }
        return deckCharacterCards;
    }

    /**
     * distribute the cards at the start of the game
     *
     * @param game
     * @param gameBuildingcards
     * @return list of edited BuildingCards
     */
    public List<BuildingCard> distributeCards(MVGame game, List<BuildingCard> gameBuildingcards) {
        for (MVPlayer player : game.getPlayers()) {
            List<BuildingCard> myCards = new ArrayList<>();
            for (int i = 0; i < 4; i++) {
                BuildingCard buildingCard = gameBuildingcards.get(i);
                myCards.add(buildingCard);
                gameBuildingcards.remove(i);
            }
            player.setBuildingCards(myCards);
            player.setNrOfCoins(2);

        }
        return gameBuildingcards;
    }

    /***
     * start game
     * @param gameId
     */
    public void startGame(int gameId) {
        //create new cards
        List<BuildingCard> gameBuildingcards = createBuildingCards();
        List<CharacterCard> gameCharacterCards = createCharacterCards();

        //add cards to game
        MVGame game = this.gameRepo.findById(gameId);
        Collections.shuffle(gameBuildingcards);
        Collections.shuffle(gameCharacterCards);
        gameCharacterCards=this.assignMiddleCards(gameCharacterCards, game.getPlayers().size());

        //Persist the cards
        buildingCardService.saveAll(gameBuildingcards);
        characterCardService.saveAll(gameCharacterCards);
        gameBuildingcards = distributeCards(game, gameBuildingcards);


        int userIndex = determineKing(game);
        game.getPlayers().get(userIndex).setHasCrown(true);
        game.setPlayerTurn(0);
        Collections.rotate(game.getPlayers(), game.getPlayers().size()-userIndex);


        game.setBuildings(gameBuildingcards);
        game.setCharacters(gameCharacterCards);
        game.setStatus(GameStatus.ONGOING);
        //save
        gameRepo.save(game);
    }

    public int determineKing(MVGame game) {
        LocalDateTime now = LocalDateTime.now();
        int userIndex = -1;
        for (int i = 0; i < game.getPlayers().size(); i++) {
            User user = userService.findByUsername(game.getPlayers().get(i).getActiveUserName());
            if (user.getDateOfBirth().isBefore(now)) {
                now = user.getDateOfBirth();
                userIndex = i;
            }
        }
        return userIndex;
    }

    public List<PlayerDto> getLobbyPlayers(int gameId) {
        ArrayList<PlayerDto> playerDtos = new ArrayList<>();
        MVGame game = this.gameRepo.findById(gameId);
        for (MVPlayer p :game.getPlayers()) {
            playerDtos.add(this.dtoMapper.toDto(p));
        }
        return playerDtos;
    }

    public PlayerDto leaveGame(String userName, int gameId) {
        MVGame gameToLeave = this.gameRepo.findById(gameId);
        User leavingUser = this.userService.findByUsername(userName);
        MVPlayer leavingPlayer = gameToLeave.getPlayers().stream().filter(p -> p.getUsers().contains(leavingUser)).findAny().orElse(null);
        gameToLeave.getPlayers().remove(leavingPlayer);
        leavingUser.getPlayers().remove(leavingPlayer);
        leavingPlayer.getUsers().remove(leavingUser);
        userService.saveUser(leavingUser);
        playerService.save(leavingPlayer);
        gameRepo.save(gameToLeave);
        return dtoMapper.toDto(leavingPlayer);
    }

    public List<String> getPreviousPlayers(String username) {
        List<GameDto> allgames = gameService.getGameOverview(username);
        List<String> usernames = new ArrayList<>();
        List<PlayerDto> allLobbyPlayers = new ArrayList<>();
        for (GameDto allgame : allgames) {
            allLobbyPlayers.addAll(this.getLobbyPlayers(allgame.getId()));
        }
        for (PlayerDto allLobbyPlayer : allLobbyPlayers) {
            usernames.add(allLobbyPlayer.getUserName());
        }

        List<String> userList=new ArrayList<>(new HashSet<>(usernames));
        userList.remove(username);
        return userList;
    }

    public boolean connectAndroid(String userName, int gameId) {
        User connectingUser = this.userService.findByUsername(userName);
        MVGame game = this.gameRepo.findById(gameId);
        if(game==null){
            return false;
        }
        return this.IsAlreadyJoined(connectingUser, game);
    }

    public List<CharacterCard> assignMiddleCards(List<CharacterCard> gameCharacterCards, int nrOfPlayers) {
        List<CharacterCard> filterKingCards = gameCharacterCards.stream().filter(c -> !c.getCharacter().getCharacterName().equals("Koning")).collect(Collectors.toList()); //we don't want the King middle
        if (nrOfPlayers <= 4) {
            filterKingCards.get(0).setFaceUp(true);
            filterKingCards.get(0).setPlacedMiddle(true);
            filterKingCards.get(1).setFaceUp(true);
            filterKingCards.get(1).setPlacedMiddle(true);
            filterKingCards.get(2).setPlacedMiddle(true); //face down middle
        }
        if (nrOfPlayers == 5) {
            filterKingCards.get(0).setFaceUp(true);
            filterKingCards.get(0).setPlacedMiddle(true);
            filterKingCards.get(1).setPlacedMiddle(true); //face down middle
        }
        if (nrOfPlayers > 5) {
            filterKingCards.get(0).setPlacedMiddle(true); //face down middle
        }
        filterKingCards.add(gameCharacterCards.stream().filter(c -> c.getCharacter().getCharacterName().equals("Koning")).findAny().orElse(null));
        return filterKingCards;
    }
}