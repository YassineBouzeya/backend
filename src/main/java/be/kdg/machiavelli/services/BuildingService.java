package be.kdg.machiavelli.services;

import be.kdg.machiavelli.model.dto.BuildingCardDto;
import be.kdg.machiavelli.model.dto.DtoMapper;
import be.kdg.machiavelli.model.game.artifacts.BuildingCard;
import be.kdg.machiavelli.model.game.artifacts.MVBuilding;
import be.kdg.machiavelli.model.game.logic.MVGame;
import be.kdg.machiavelli.repositories.MVBuildingRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class BuildingService {
    @Autowired
    private MVBuildingRepo buildingRepo;

    @Autowired
    private GameService gameService;

    @Autowired
    private DtoMapper dtoMapper;

    public List<BuildingCardDto> getBuildingsCardDtosOfGame(int gameId){
        List<BuildingCardDto> myList = new ArrayList<>();
        MVGame game = gameService.getGame(gameId);
        for (BuildingCard buildingCard : game.getBuildings()){
            myList.add(dtoMapper.toDto(buildingCard));
        }
        return myList;
    }

    //this is only used for filling up the database
    public MVBuilding saveBuilding(MVBuilding building){
        return this.buildingRepo.save(building);
    }

    public MVBuilding getBuilding(int id){
        return this.buildingRepo.getOne(id);
    }

    public List<MVBuilding> findAll(){
       return buildingRepo.findAll();
    }
}
