package be.kdg.machiavelli.services;

import be.kdg.machiavelli.model.dto.CharacterCardDto;
import be.kdg.machiavelli.model.dto.DtoMapper;
import be.kdg.machiavelli.model.dto.GameDto;
import be.kdg.machiavelli.model.game.artifacts.BuildingCard;
import be.kdg.machiavelli.model.game.artifacts.CharacterCard;
import be.kdg.machiavelli.repositories.CharacterCardRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class CharacterCardService {
    @Autowired
    private CharacterCardRepo characterCardRepo;
    @Autowired
    private DtoMapper dtoMapper;

    public List<CharacterCard> saveAll(List characterCard) {
        return this.characterCardRepo.saveAll(characterCard);
    }

    public CharacterCard getOneCharacterCard(int id) {
        return this.characterCardRepo.findById(id).get();
    }

    public List<CharacterCard> getAllCharacterCards() {
        return characterCardRepo.findAll();
    }

    public CharacterCardDto getOneCharacterCardDto(int id) {
        return this.dtoMapper.toDto(this.characterCardRepo.findById(id).get());
    }
}
