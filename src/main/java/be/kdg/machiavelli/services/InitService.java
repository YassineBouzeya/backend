package be.kdg.machiavelli.services;

import be.kdg.machiavelli.model.game.artifacts.MVBuilding;
import be.kdg.machiavelli.model.game.artifacts.MVCharacter;
import be.kdg.machiavelli.model.game.logic.Color;
import be.kdg.machiavelli.repositories.MVBuildingRepo;
import be.kdg.machiavelli.repositories.MVCharacterRepo;
import org.springframework.stereotype.Service;

@Service
public class InitService {
    private BuildingService buildingService;
    private CharacterService characterService;

    public InitService(BuildingService buildingService, CharacterService characterService) {
        this.buildingService = buildingService;
        this.characterService = characterService;
    }

    private MVCharacter[] characters() {
        return new MVCharacter[]{
                new MVCharacter("Moordenaar", "http://i65.tinypic.com/i3xoic.jpg", Color.GREY),
                new MVCharacter("Dief", "http://i65.tinypic.com/ff4ymr.jpg", Color.GREY),
                new MVCharacter("Magier", "http://i65.tinypic.com/2h85u3a.jpg", Color.GREY),
                new MVCharacter("Koning", "http://i64.tinypic.com/2vklv1k.jpg", Color.YELLOW),
                new MVCharacter("Prediker", "http://i64.tinypic.com/2vklv1k.jpg", Color.BLUE),
                new MVCharacter("Koopman", "http://i64.tinypic.com/nd375w.jpg", Color.GREEN),
                new MVCharacter("Bouwmeester", "http://i67.tinypic.com/nv64he.jpg", Color.GREY),
                new MVCharacter("Condottiere", "http://i66.tinypic.com/do5b3p.jpg", Color.RED)
        };
    }

    private MVBuilding[] buildings() {
        return new MVBuilding[]{
                new MVBuilding("Abdij", "http://i63.tinypic.com/htb6fl.jpg", Color.BLUE, 3,3),
                new MVBuilding("Gildenhuis", "http://i64.tinypic.com/n8554.jpg", Color.GREEN, 2,3),
                new MVBuilding("Handelshuis", "http://i64.tinypic.com/51vsp4.jpg", Color.GREEN, 3,3),
                new MVBuilding("Haven", "http://i68.tinypic.com/1gj11w.jpg", Color.GREEN, 4,3),
                new MVBuilding("Jachtslot", "http://i64.tinypic.com/2n239fr.jpg", Color.YELLOW, 3,5),
                new MVBuilding("Kathedraal", "http://i68.tinypic.com/avsexe.jpg", Color.BLUE, 5,2),
                new MVBuilding("Kerk", "http://i63.tinypic.com/kcxij6.jpg", Color.BLUE, 2,3),
                new MVBuilding("Markt", "http://i63.tinypic.com/2s63qy8.jpg", Color.GREEN, 2,4),
                new MVBuilding("Slot", "http://i64.tinypic.com/6hhyip.jpg", Color.YELLOW, 4,4),
                new MVBuilding("Paleis", "http://i63.tinypic.com/2ex8rns.jpg", Color.YELLOW, 5,3),
                new MVBuilding("Raadhuis", "http://i65.tinypic.com/eqb3gx.jpg", Color.GREEN, 5,2),
                new MVBuilding("Taveerne", "http://i67.tinypic.com/1zcohea.jpg", Color.GREEN, 1,5),
                new MVBuilding("Tempel", "http://i67.tinypic.com/2zdzlva.jpg", Color.BLUE, 1,3),
                new MVBuilding("Toernooiveld", "http://i68.tinypic.com/33v22rt.jpg", Color.RED, 3,3),
                new MVBuilding("Vesting", "http://i68.tinypic.com/w9fv3a.jpg", Color.RED, 5,2),
                new MVBuilding("Wachttoren", "http://i65.tinypic.com/2wozsq1.jpg", Color.RED, 1,3),
                new MVBuilding("Kerker", "http://i66.tinypic.com/e157xh.jpg", Color.RED, 2,3)
        };
    }

    public void persistCharacters() {
        /*
        for (MVCharacter character : characters()) {
            characterService.saveCharacter(character);
        }
        */
    }

    public void persistBuildings(){
        /*
        for (MVBuilding building : buildings()) {
            buildingService.saveBuilding(building);
        }
        */
    }


}
