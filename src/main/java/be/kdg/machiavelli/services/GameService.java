package be.kdg.machiavelli.services;

import be.kdg.machiavelli.exceptions.MyException;
import be.kdg.machiavelli.model.dto.*;
import be.kdg.machiavelli.model.game.artifacts.BuildingCard;
import be.kdg.machiavelli.model.game.artifacts.CharacterCard;
import be.kdg.machiavelli.model.game.logic.*;
import be.kdg.machiavelli.model.security.User;
import be.kdg.machiavelli.repositories.MVGameRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class GameService {
    private static final Logger LOGGER = LoggerFactory.getLogger(GameService.class);

    private final MVGameRepo gameRepo;
    private final PlayerService playerService;
    private final UserService userDetailsService;
    private DtoMapper dtoMapper;
    private CharacterCardService characterCardService;
    private BuildingCardService buildingCardService;


    @Autowired
    public GameService(MVGameRepo gameRepo, PlayerService playerService, UserService userService,
                       DtoMapper dtoMapper, CharacterCardService characterCardService, BuildingCardService buildingCardService) {
        this.gameRepo = gameRepo;
        this.playerService = playerService;
        this.userDetailsService = userService;
        this.dtoMapper = dtoMapper;
        this.characterCardService = characterCardService;
        this.buildingCardService = buildingCardService;
    }

    /***
     * persists a game
     * @param game
     * @return
     */
    public MVGame saveGame(MVGame game) {
        return this.gameRepo.save(game);
    }

    /***
     * returns a game when given a gameID
     * @param id
     * @return
     */
    public MVGame getGame(int id) {
        return gameRepo.getOne(id);
    }

    /***
     * returns a gameDTO when given a gameID
     * @param id
     * @return
     */
    public GameDto getGameDto(int id) {
        return this.dtoMapper.toDto(this.getGame(id));
    }

    /***
     * This method returns the entire game history of played, participated or currently running games
     * @param userName
     * @return list of games
     */
    public List<GameDto> getGameOverview(String userName) {
        User user = this.userDetailsService.findByUsername(userName);
        List<GameDto> allUserGames = new ArrayList<>();
        List<MVPlayer> userPlayers = this.playerService.findAllByUsersContains(user);
        for (MVPlayer player : userPlayers) {
            allUserGames.add(dtoMapper.toLiteGameDto(this.gameRepo.findByPlayersContains(player)));
        }
        return allUserGames;
    }

    /***
     * searches for games that are open and in a lobby status
     * @return
     */
    public List<MVGame> searchGames() {
        return gameRepo.findAllByJoinTypeAndStatus(JoinType.OPEN, GameStatus.LOBBY);
    }


    /***
     * searches for games that are open and in a lobby status but returns them in DTO form
     * @return dto list of games
     */
    public List<GameDto> searchGameDtos() {
        List<GameDto> gameDtos = new ArrayList<>();
        List<MVGame> games = this.searchGames();
        for (MVGame g : games
        ) {
            gameDtos.add(dtoMapper.toLiteGameDto(g));
        }
        return gameDtos;
    }

    /***
     * returns a the characters of a given game
     * @param gameId
     * @return
     * @throws MyException
     */
    public List<CharacterCard> getCharactersOfGame(int gameId) throws MyException {
        try {
            return getGame(gameId).getCharacters();
        } catch (EntityNotFoundException | IndexOutOfBoundsException e) {
            LOGGER.error("GameId does not have characters " + e.getMessage());
            throw new MyException("the character was not found", e);
        }

    }

    /***
     * returns DTO list of characters from a game using the getCharactersOfGame() method
     * @param gameId
     * @return
     */
    public List<CharacterCardDto> getCharacterCardDtosOfGame(int gameId) throws MyException {
        List<CharacterCardDto> myList = new ArrayList<>();
        for (CharacterCard characterCard : getCharactersOfGame(gameId)) {
            myList.add(dtoMapper.toDto(characterCard));
        }
        return myList;
    }


    /***
     * Method that dertermines if the king has been chosen in a particular round
     * @param gameId
     * @return
     */
    public int determineKing(int gameId) throws MyException {
        try {
            MVGame game = getGame(gameId);
            int kingId = 4;
            int index = -1;
            for (int i = 0; i < game.getPlayers().size(); i++) {
                MVPlayer player = game.getPlayers().get(i);
                if (player.getCharacterCard().getCharacter().getId() == kingId) {
                    index = i;
                }
            }
            return index;
            //if return is -1 => the king was not chosen in the last round
        } catch (NullPointerException | IndexOutOfBoundsException e) {
            LOGGER.error("Exception in determineKing: " + e.getMessage());
            throw new MyException("Error in the process of determining the king", e);
        }


    }

    /***
     * Method that will assign the the chosen character of a player to the player
     * @param changeCaracterDto
     * @return
     */
    public GameDto assignCharacterToPlayer(ChangeCaracterDto changeCaracterDto) {
        GameDto game = changeCaracterDto.getGameDto();
        CharacterCard characterCard = characterCardService.getOneCharacterCard(changeCaracterDto.getCharacterCardId());
        Arrays.stream(game.getPlayers()).filter(p -> p.getOrderNr() == changeCaracterDto.getPlayerId()).findFirst().orElse(null).setCharacterCard(dtoMapper.toDto(characterCard));
        if (game.getPlayerTurn() == (game.getPlayers().length - 1)) {
            game.setPlayersChosen(true);
        } else {
            game.setPlayersChosen(false);
        }
        game.setPlayerTurn(nextPlayer(game, changeCaracterDto.getPlayerId()));
        return game;
    }

    /***
     *  method that will determine the next round's player turn
     * @param game
     * @param orderNr
     * @return
     */
    public int nextPlayer(GameDto game, int orderNr) {
        orderNr++;
        if (orderNr >= game.getPlayers().length) {
            if (game.getPlayers()[0].getCharacterCard() != null) {
                if (game.getPlayers()[0].getCharacterCard().isKilled()) {
                    return nextPlayer(game, 0);
                }
            }

            return 0;
        } else {
            if (game.getPlayers()[orderNr].getCharacterCard() != null) {
                if (game.getPlayers()[orderNr].getCharacterCard().isKilled()) {
                    return nextPlayer(game, orderNr);
                }
            }

            return orderNr;
        }
    }


    /***
     * This method will determine what character within the game will play next
     * @param game
     * @return
     */
    public GameDto nextCharacter(GameDto game) {
        if (game.getPlayers()[0].getCharacterCard() != null) {
            int characterTurn = game.getCharacterTurn();
            int nextCharacter = this.whoPlaysNext(characterTurn);
            if (nextCharacter >= 9) {
                //Warlord was chosen
                game = this.resetRound(game);
                //PlayerTurn 0 because the first player needs to select the character for the next round
                game.setPlayerTurn(0);
                return game;
            } else {
                //chose the next character to play
                game = this.nextCharacterToPlay(game, nextCharacter);
                if (game.getCharacterTurn() != nextCharacter) {
                    game.setCharacterTurn(nextCharacter);
                    game = nextCharacter(game);
                }
            }
        }
        return game;
    }

    private GameDto nextCharacterToPlay(GameDto game, int nextCharacter) {
        for (int i = 0; i < game.getPlayers().length; i++) {
            if (game.getPlayers()[i].getCharacterCard().getCharacter().getId() == nextCharacter) {
                if (!game.getPlayers()[i].getCharacterCard().isKilled()) {
                    if (game.getPlayers()[i].getCharacterCard().getCharacter().getCharacterName().equalsIgnoreCase("Koning")) {
                        game.getPlayers()[i].setHasCrown(true);
                        for (int j = 0; j < game.getPlayers().length; j++) {
                            if (j != i) {
                                game.getPlayers()[j].setHasCrown(false);
                            }
                        }
                    }
                    game.setPlayerTurn(game.getPlayers()[i].getOrderNr());
                    game.setCharacterTurn(nextCharacter);

                    return game;
                }
            }
        }
        return game;
    }

    private int whoPlaysNext(int lastCharacter) {
        if (lastCharacter == -1) {
            return 1;
        }
        if (lastCharacter >= 0 && lastCharacter < 9) {
            return lastCharacter + 1;
        } else return -1;
    }

    public GameDto resetRound(GameDto game) {
        if (this.checkReqBuildingsMet(game)) {
            game.setCharacterTurn(-1);
            game.setGameOver(true);
            game.setStatus(2);
            game = this.endgame(game);
            saveGame(game);
            return game;
        }
        int playerLength = game.getPlayers().length;
        game.setCharacterTurn(1);
        CharacterCardDto[] characterCardDtos = new CharacterCardDto[8];
        for (int i = 0; i < game.getCharacterCards().length; i++) {
            characterCardDtos[i] = game.getCharacterCards()[i];
        }
        for (int i = 0; i < playerLength; i++) {
            characterCardDtos[game.getCharacterCards().length + i] = game.getPlayers()[i].getCharacterCard();
            if (game.getPlayers()[i].getCharacterCard().getCharacter().getCharacterName().equals("Koning")) {
                for (int j = 0; j < playerLength; j++) {
                    if (j < i) {
                        game.getPlayers()[j].setOrderNr(playerLength - 1 - j);
                    }
                    if (j > i) {
                        game.getPlayers()[j].setOrderNr(j - i);
                    }
                }
                game.getPlayers()[i].setOrderNr(0);
            }
            game.getPlayers()[i].setCharacterCard(null);
        }
        for (CharacterCardDto characterCard : characterCardDtos) {
            characterCard.setKilled(false);
            characterCard.setRobbed(false);
            characterCard.setPlacedMiddle(false);
            characterCard.setFaceUp(false);
        }
        List<CharacterCardDto> characterCardDtoList = Arrays.asList(characterCardDtos);
        Collections.shuffle(characterCardDtoList);
        characterCardDtoList = this.assignMiddleCardsDto(characterCardDtoList, game.getPlayers().length);
        characterCardDtos = characterCardDtoList.toArray(new CharacterCardDto[0]);
        game.setCharacterCards(characterCardDtos);
        game.setCharacterTurn(-1);
        game.setPlayersChosen(false);
        return game;
    }

    private boolean checkReqBuildingsMet(GameDto game) {
        for (PlayerDto p : game.getPlayers()) {
            if (Arrays.stream(p.getBuildingCards()).filter(b -> b.isBought()).count() >= game.getReqBuildings()) {
                return true;
            }
        }
        return false;
    }

    public GameDto getLiteGameDto(int gameId) {
        GameDto gameDto = null;
        try {
            gameDto = this.dtoMapper.toLiteGameDto(this.getGame(gameId));
        } catch (EntityNotFoundException e) {
            LOGGER.info("Game with id " + gameId + "not found. Probably a non existing gameId was entered in a url.");
        }
        return gameDto;
    }

    public GameDto endgame(GameDto gameDto) {
        gameDto.getPlayers();
        int points = 0;
        for (PlayerDto p : gameDto.getPlayers()) {
            HashSet<Integer> colors = new HashSet<>();
            // points equivalent to the cost of every bought building
            for (BuildingCardDto b : p.getBuildingCards()) {
                if (b.isBought()) {
                    points += b.getBuilding().getCost();
                    colors.add(b.getBuilding().getColor());
                }
            }
            // 3 points if a players has bought buildings of all colors
            if (colors.size() == 4) {
                points += 3;
            }
            // 4 points if the player was the first to get the required amount of buildings
            if (p.isFirstWithReqBuildings()) {
                points += 4;
            }
            // 2 points if the player was not first but still has the required amount
            else if (Arrays.stream(p.getBuildingCards()).filter(b -> b.isBought()).count() >= gameDto.getReqBuildings()) {
                points += 2;
            }

            p.setEndGamePoints(points);
        }
        return gameDto;
    }

    public List<CharacterCardDto> assignMiddleCardsDto(List<CharacterCardDto> gameCharacterCards, int nrOfPlayersInGame) {
        //Cards are shuffled already
        List<CharacterCardDto> filterKingCardsofGame = gameCharacterCards.stream().filter(ca -> !ca.getCharacter().getCharacterName().equals("Koning")).collect(Collectors.toList()); //we don't want the King middle
        if (nrOfPlayersInGame <= 4) {
            filterKingCardsofGame.get(2).setPlacedMiddle(true);
            filterKingCardsofGame.get(1).setFaceUp(true);
            filterKingCardsofGame.get(1).setPlacedMiddle(true);
            filterKingCardsofGame.get(0).setFaceUp(true);
            filterKingCardsofGame.get(0).setPlacedMiddle(true);
        }

        if (nrOfPlayersInGame > 5) {
            filterKingCardsofGame.get(0).setPlacedMiddle(true); //face down middle
        }

        if (nrOfPlayersInGame == 5) {
            filterKingCardsofGame.get(0).setPlacedMiddle(true);
            filterKingCardsofGame.get(0).setFaceUp(true);
            filterKingCardsofGame.get(1).setPlacedMiddle(true); //face down middle
        }
        filterKingCardsofGame.add(gameCharacterCards.stream().filter(ca -> ca.getCharacter().getCharacterName().equals("Koning")).findAny().orElse(null));
        return filterKingCardsofGame;
    }

    public void saveGame(GameDto gameDto) {
        MVGame game = this.getGame(gameDto.getId());
        game.setName(gameDto.getGameName());
        switch (gameDto.getStatus()) {
            case 0:
                game.setStatus(GameStatus.LOBBY);
                break;
            case 1:
                game.setStatus(GameStatus.ONGOING);
                break;
            case 2:
                game.setStatus(GameStatus.ENDED);
                break;
        }
        game.setCoinsLeft(game.getCoinsLeft());
        game.setTurnDuration(gameDto.getTurnDuration());
        game.setReqBuildings(gameDto.getReqBuildings());
        game.setMaxPlayers(gameDto.getMaxPlayers());
        game.setCharacterTurn(gameDto.getCharacterTurn());
        game.setPlayersChosen(gameDto.isPlayersChosen());
        switch (gameDto.getJoinType()) {
            case 0:
                game.setJoinType(JoinType.OPEN);
                break;
            case 1:
                game.setJoinType(JoinType.PRIVATE);
                break;
        }
        game.setPlayerTurn(gameDto.getPlayerTurn());

        //region Players
        ArrayList<MVPlayer> gamePlayers = new ArrayList<>();
        for (PlayerDto playerDto : gameDto.getPlayers()) {
            MVPlayer player = this.playerService.findOnePlayerById(playerDto.getId());
            if (playerDto.getCharacterCard() != null) {
                CharacterCard characterCard = characterCardService.getOneCharacterCard(playerDto.getCharacterCard().getId());
                player.setCharacterCard(this.dtoMapper.fromDto(characterCard, playerDto.getCharacterCard()));
            }
            ArrayList<BuildingCard> myBuildings = new ArrayList<>();
            if (playerDto.getBuildingCards() != null) {
                for (BuildingCardDto buildingCardDto : playerDto.getBuildingCards()) {
                    if (buildingCardDto != null) {
                        BuildingCard buildingCard =
                                this.buildingCardService.getOneBuildingCard(buildingCardDto.getId());
                        myBuildings.add(this.dtoMapper.fromDto(buildingCard, buildingCardDto));
                    }
                }
            }
            player.setBuildingCards(myBuildings);
            player.setHasCrown(playerDto.isHasCrown());
            player.setOrderNr(playerDto.getOrderNr());
            player.setNrOfCoins(playerDto.getNrOfCoins());
            player.setEndGamePoints(playerDto.getEndGamePoints());
            player.setHost(playerDto.isHost());
            player.setFirstWithReqBuildings(playerDto.isFirstWithReqBuildings());
            gamePlayers.add(player);
        }
        game.setPlayers(gamePlayers);
        //endregion

        //region Buildings
        ArrayList<BuildingCard> gameBuildings = new ArrayList<>();
        for (BuildingCardDto buildingCardDto : gameDto.getBuildingCards()) {
            BuildingCard buildingCard = this.buildingCardService.getOneBuildingCard(buildingCardDto.getId());
            gameBuildings.add(dtoMapper.fromDto(buildingCard, buildingCardDto));
        }
        game.setBuildings(gameBuildings);
        //endregion

        //region Characters
        ArrayList<CharacterCard> gameCharacters = new ArrayList<>();
        for (CharacterCardDto characterCardDto : gameDto.getCharacterCards()) {
            CharacterCard characterCard = characterCardService.getOneCharacterCard(characterCardDto.getId());
            gameCharacters.add(dtoMapper.fromDto(characterCard, characterCardDto));
        }
        game.setCharacters(gameCharacters);
        //endregion
        this.saveGame(game);
    }
}
