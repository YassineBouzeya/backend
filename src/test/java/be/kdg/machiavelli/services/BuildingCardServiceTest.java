package be.kdg.machiavelli.services;

import be.kdg.machiavelli.model.game.artifacts.BuildingCard;
import be.kdg.machiavelli.model.game.artifacts.MVBuilding;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class BuildingCardServiceTest {
    @Autowired
    BuildingCardService buildingCardService;

    @Autowired
    BuildingService buildingService;

    @Test
    @Transactional
    public void BuildingsServiceScenario() {
        int amount = buildingCardService.findAll().size();
        buildingCardService.saveBuilding(new BuildingCard());
        assertNotNull(buildingCardService.findAll());
        List<BuildingCard> listToAdd = new ArrayList<>();
        listToAdd.add(new BuildingCard());
        listToAdd.add(new BuildingCard());
        buildingCardService.saveAll(listToAdd);
        assertEquals(buildingCardService.findAll().size(),  amount+3);
        buildingCardService.removeBuildingById(listToAdd.get(0).getId());
        assertEquals(buildingCardService.findAll().size(),  amount +2);
    }

    @Test
    @Transactional
    public void BuildingsServiceScenario2() {
        //buildingCardService.removeBuildingAll();
        MVBuilding building = buildingService.getBuilding(1);
        BuildingCard buildingCard = new BuildingCard(building);
        buildingCard.setDiscarded(false);
        buildingCardService.saveBuilding(buildingCard);
        assertNotNull(buildingCardService.findAll());
        buildingCardService.discardBuilding(buildingCard.getId());
        assertTrue(buildingCardService.getOneBuildingCard(buildingCard.getId()).isDiscarded());
    }

    @Test
    @Transactional
    public void getBuildingsCardDtosOfGame() {
        List<BuildingCard> list = buildingCardService.findAll();
        assertNotNull(list);

    }

    @Test
    @Transactional
    public void addBuildingsCardDtosOfGame() {
        List<BuildingCard> list = buildingCardService.findAll();
        int amount = list.size();
        buildingCardService.saveBuilding(new BuildingCard());
        assertEquals(buildingCardService.findAll().size(), amount + 1);
    }

    @Test
    @Transactional
    public void addMoreBuildings() {
        List<BuildingCard> list = buildingCardService.findAll();
        int amaount = list.size();
        List<BuildingCard> listToAdd = new ArrayList<>();
        listToAdd.add(new BuildingCard());
        listToAdd.add(new BuildingCard());
        listToAdd.add(new BuildingCard());
        listToAdd.add(new BuildingCard());
        buildingCardService.saveAll(listToAdd);
        assertEquals(buildingCardService.findAll().size(), amaount + 4);
    }
    @Test
    @Transactional
    public void saveAndFindABuilding() {
       buildingCardService.saveBuilding(new BuildingCard());
       BuildingCard b =  buildingCardService.getOneBuildingCard(0);
       assertNotNull(b);
    }




}