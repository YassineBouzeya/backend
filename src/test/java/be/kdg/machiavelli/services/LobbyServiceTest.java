package be.kdg.machiavelli.services;

import be.kdg.machiavelli.model.dto.GameDto;
import be.kdg.machiavelli.model.dto.PlayerDto;
import be.kdg.machiavelli.model.game.artifacts.BuildingCard;
import be.kdg.machiavelli.model.game.logic.MVGame;
import be.kdg.machiavelli.model.security.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class LobbyServiceTest {
    @Autowired
    private LobbyService lobbyService;


    @Mock
    private PlayerDto playerDto;
    @Mock
    private GameDto gameDto;
    @Mock
    private MVGame mvGame;
    @Mock
    private User user;
    @Mock
    private BuildingCard buildingCard;

    ArrayList<PlayerDto> playerDtos = new ArrayList<>();
    @Before
    public void setup() {

     }

    @Test
    public void testMakeLobby() {
        assertNotNull("lobbyService should never return null", lobbyService.makeLobby(gameDto));
    }


    @Test
    @Transactional
    public void joinLobby() {
        assertNotNull("lobbyService should never return null", lobbyService.joinLobby("test", 0));
    }

    @Test
    @Transactional
    public void IsAlreadyJoined() {
        assertNotNull("lobbyService should never return null", lobbyService.IsAlreadyJoined(user, mvGame));
    }

    @Test
    @Transactional
    public void createBuildingCards() {
        assertNotNull("lobbyService should never return null", lobbyService.createBuildingCards());
    }

    @Test
    @Transactional
    public void distributeCards() {
        List b = new ArrayList<>();
        b.add(buildingCard);
        assertNotNull("lobbyService should never return null", lobbyService.distributeCards(mvGame, b));
    }

}