package be.kdg.machiavelli.controllers;

import be.kdg.machiavelli.MachiavelliApplication;
import be.kdg.machiavelli.model.game.artifacts.MVCharacter;
import be.kdg.machiavelli.services.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;

import java.beans.Transient;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringBootTest(classes = MachiavelliApplication.class)
public class CardsControllerTest {
    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private FilterChainProxy springSecurityFilterChain;

    @Autowired
    private UserService userService;

    @Autowired
    BuildingService buildingService;

    @Autowired
    GameService gameService;

    private MockMvc mockMvc;

    private static final String CLIENT_ID = "jwtclientid";
    private static final String CLIENT_SECRET = "XY7kmzoNzl100";

    private static final String CONTENT_TYPE = "application/json;charset=UTF-8";

    private static final String LOGIN = "yassine.bouzeya";
    //private static final String NAME = "Yassine";

    @Before
    @Transactional
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).addFilter(springSecurityFilterChain).build();

    }

    private String obtainAccessToken(String username, String password) throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).addFilter(springSecurityFilterChain).build();

        final MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("grant_type", "password");
        params.add("username", username);
        params.add("password", password);

        ResultActions result = mockMvc.perform(post("/oauth/token")
                .params(params)
                .with(httpBasic(CLIENT_ID, CLIENT_SECRET))
                .accept(CONTENT_TYPE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(CONTENT_TYPE));


        String resultString = result.andReturn().getResponse().getContentAsString();

        JacksonJsonParser jsonParser = new JacksonJsonParser();
        return jsonParser.parseMap(resultString).get("access_token").toString();
    }

    @Autowired
    private CardsController cardsController;

    @MockBean
    CharacterService characterService;

    @Test
    public void contexLoads() throws Exception {
        assertNotNull(cardsController);
    }

    @Test
    @Transactional
    public void shouldReturnFail() throws Exception {
        final String accessToken = obtainAccessToken("yassine.bouzeya", "jwtpass");

        MVCharacter character = new MVCharacter();
        characterService.saveCharacter(character);
        when(characterService.getCharacter(1)).thenReturn(character);
        this.mockMvc.perform(get("/buildings/1371")
                .header("Authorization", "Bearer " + accessToken)
                .contentType(CONTENT_TYPE)
                .content("1371"))
                .andDo(print()).andExpect(status().isIAmATeapot());
    }



    @Test
    @Transactional
    public void shouldReturnDefault() throws Exception {
        final String accessToken = obtainAccessToken("yassine.bouzeya", "jwtpass");
//        int gameid = gameService.getGameOverview("yassine.bouzeya").get(0).getId();
        int gameId = gameService.searchGames().get(1).getId();
        int buildid = buildingService.getBuildingsCardDtosOfGame(gameId).get(1).getId();
        //int buildid = buildingService.getBuildingsCardDtosOfGame(gameId).get(1).getId();
        MVCharacter character = new MVCharacter();
        characterService.saveCharacter(character);

        this.mockMvc.perform(get("/buildings/" + buildid)
                .header("Authorization", "Bearer " + accessToken)
                .contentType(CONTENT_TYPE))
                .andDo(print()).andExpect(status().isOk());
    }
}